Index: node_modules/puppeteer-core/lib/Page.js
===================================================================
--- node_modules/puppeteer-core/lib/Page.js
+++ node_modules/puppeteer-core/lib/Page.js
@@ -124,8 +124,10 @@
     this._frameManager.on(FrameManager.Events.FrameNavigated, event => this.emit(Page.Events.FrameNavigated, event));
 
     this._networkManager.on(NetworkManager.Events.Request, event => this.emit(Page.Events.Request, event));
     this._networkManager.on(NetworkManager.Events.Response, event => this.emit(Page.Events.Response, event));
+    this._networkManager.on(NetworkManager.Events.DataReceived, event => this.emit(Page.Events.DataReceived, event));
+    this._networkManager.on(NetworkManager.Events.RequestIdle, event => this.emit(Page.Events.RequestIdle, event));
     this._networkManager.on(NetworkManager.Events.RequestFailed, event => this.emit(Page.Events.RequestFailed, event));
     this._networkManager.on(NetworkManager.Events.RequestFinished, event => this.emit(Page.Events.RequestFinished, event));
 
     client.on('Page.domContentEventFired', event => this.emit(Page.Events.DOMContentLoaded));
@@ -1144,8 +1146,10 @@
   // @see https://nodejs.org/api/events.html#events_error_events
   PageError: 'pageerror',
   Request: 'request',
   Response: 'response',
+  DataReceived: 'datareceived',
+  RequestIdle: 'requestidle',
   RequestFailed: 'requestfailed',
   RequestFinished: 'requestfinished',
   FrameAttached: 'frameattached',
   FrameDetached: 'framedetached',
