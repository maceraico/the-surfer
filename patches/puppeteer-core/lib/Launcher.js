Index: node_modules/puppeteer-core/lib/Launcher.js
===================================================================
--- node_modules/puppeteer-core/lib/Launcher.js
+++ node_modules/puppeteer-core/lib/Launcher.js
@@ -32,24 +32,26 @@
 
 const DEFAULT_ARGS = [
   '--disable-background-networking',
   '--disable-background-timer-throttling',
+  '--disable-backgrounding-occluded-windows',
   '--disable-breakpad',
   '--disable-client-side-phishing-detection',
+  '--disable-component-extensions-with-background-pages',
   '--disable-default-apps',
   '--disable-dev-shm-usage',
   '--disable-extensions',
-  // TODO: Support OOOPIF. @see https://github.com/GoogleChrome/puppeteer/issues/2548
-  '--disable-features=site-per-process',
+  // BlinkGenPropertyTrees disabled due to crbug.com/937609
+  '--disable-features=TranslateUI,BlinkGenPropertyTrees',
   '--disable-hang-monitor',
+  '--disable-ipc-flooding-protection',
   '--disable-popup-blocking',
   '--disable-prompt-on-repost',
+  '--disable-renderer-backgrounding',
   '--disable-sync',
-  '--disable-translate',
+  '--force-color-profile=srgb',
   '--metrics-recording-only',
   '--no-first-run',
-  '--safebrowsing-disable-auto-update',
-  '--enable-automation',
   '--password-store=basic',
   '--use-mock-keychain',
 ];
 
