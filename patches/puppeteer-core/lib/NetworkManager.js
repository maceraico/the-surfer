Index: node_modules/puppeteer-core/lib/NetworkManager.js
===================================================================
--- node_modules/puppeteer-core/lib/NetworkManager.js
+++ node_modules/puppeteer-core/lib/NetworkManager.js
@@ -14,9 +14,8 @@
  * limitations under the License.
  */
 const EventEmitter = require('events');
 const {helper, assert, debugError} = require('./helper');
-const Multimap = require('./Multimap');
 
 class NetworkManager extends EventEmitter {
   /**
    * @param {!Puppeteer.CDPSession} client
@@ -27,8 +26,9 @@
     this._client = client;
     this._frameManager = frameManager;
     /** @type {!Map<string, !Request>} */
     this._requestIdToRequest = new Map();
+    this._requestIdToResponseEvent = new Map();
     /** @type {!Map<string, !Protocol.Network.requestWillBeSentPayload>} */
     this._requestIdToRequestWillBeSentEvent = new Map();
     /** @type {!Object<string, string>} */
     this._extraHTTPHeaders = {};
@@ -40,16 +40,14 @@
     /** @type {!Set<string>} */
     this._attemptedAuthentications = new Set();
     this._userRequestInterceptionEnabled = false;
     this._protocolRequestInterceptionEnabled = false;
-    /** @type {!Multimap<string, string>} */
-    this._requestHashToRequestIds = new Multimap();
-    /** @type {!Multimap<string, string>} */
-    this._requestHashToInterceptionIds = new Multimap();
+    this._requestIdToInterceptionId = new Map();
 
     this._client.on('Network.requestWillBeSent', this._onRequestWillBeSent.bind(this));
     this._client.on('Network.requestIntercepted', this._onRequestIntercepted.bind(this));
     this._client.on('Network.requestServedFromCache', this._onRequestServedFromCache.bind(this));
+    this._client.on('Network.dataReceived', this._onDataReceived.bind(this));
     this._client.on('Network.responseReceived', this._onResponseReceived.bind(this));
     this._client.on('Network.loadingFinished', this._onLoadingFinished.bind(this));
     this._client.on('Network.loadingFailed', this._onLoadingFailed.bind(this));
   }
@@ -119,9 +117,8 @@
       return;
     this._protocolRequestInterceptionEnabled = enabled;
     const patterns = enabled ? [{urlPattern: '*'}] : [];
     await Promise.all([
-      this._client.send('Network.setCacheDisabled', {cacheDisabled: enabled}),
       this._client.send('Network.setRequestInterception', {patterns})
     ]);
   }
 
@@ -129,15 +126,14 @@
    * @param {!Protocol.Network.requestWillBeSentPayload} event
    */
   _onRequestWillBeSent(event) {
     if (this._protocolRequestInterceptionEnabled) {
-      const requestHash = generateRequestHash(event.request);
-      const interceptionId = this._requestHashToInterceptionIds.firstValue(requestHash);
+      const requestId = event.requestId;
+      const interceptionId = this._requestIdToInterceptionId.get(requestId);
       if (interceptionId) {
         this._onRequest(event, interceptionId);
-        this._requestHashToInterceptionIds.delete(requestHash, interceptionId);
+        this._requestIdToInterceptionId.delete(requestId);
       } else {
-        this._requestHashToRequestIds.set(requestHash, event.requestId);
         this._requestIdToRequestWillBeSentEvent.set(event.requestId, event);
       }
       return;
     }
@@ -150,15 +146,19 @@
   _onRequestIntercepted(event) {
     if (event.authChallenge) {
       /** @type {"Default"|"CancelAuth"|"ProvideCredentials"} */
       let response = 'Default';
+      let request = {};
+      if (this._requestIdToRequest.has(event.requestId)) {
+        request = this._requestIdToRequest.get(event.requestId);
+      }
       if (this._attemptedAuthentications.has(event.interceptionId)) {
         response = 'CancelAuth';
-      } else if (this._credentials) {
+      } else if (request._credentials || this._credentials) {
         response = 'ProvideCredentials';
         this._attemptedAuthentications.add(event.interceptionId);
       }
-      const {username, password} = this._credentials || {username: undefined, password: undefined};
+      const {username, password} = request._credentials || this._credentials || {username: undefined, password: undefined};
       this._client.send('Network.continueInterceptedRequest', {
         interceptionId: event.interceptionId,
         authChallengeResponse: { response, username, password }
       }).catch(debugError);
@@ -169,17 +169,21 @@
         interceptionId: event.interceptionId
       }).catch(debugError);
     }
 
-    const requestHash = generateRequestHash(event.request);
-    const requestId = this._requestHashToRequestIds.firstValue(requestHash);
-    if (requestId) {
+    const requestId = event.requestId;
+    if (this._requestIdToRequest.has(requestId)) {
+      this._client.send('Network.continueInterceptedRequest', {
+        interceptionId: event.interceptionId
+      }).catch(debugError);
+      return;
+    }
+    if (requestId && this._requestIdToRequestWillBeSentEvent.has(requestId)) {
       const requestWillBeSentEvent = this._requestIdToRequestWillBeSentEvent.get(requestId);
       this._onRequest(requestWillBeSentEvent, event.interceptionId);
-      this._requestHashToRequestIds.delete(requestHash, requestId);
       this._requestIdToRequestWillBeSentEvent.delete(requestId);
     } else {
-      this._requestHashToInterceptionIds.set(requestHash, event.interceptionId);
+      this._requestIdToInterceptionId.set(requestId, event.interceptionId);
     }
   }
 
   /**
@@ -199,8 +203,13 @@
     const frame = event.frameId ? this._frameManager.frame(event.frameId) : null;
     const request = new Request(this._client, frame, interceptionId, this._userRequestInterceptionEnabled, event, redirectChain);
     this._requestIdToRequest.set(event.requestId, request);
     this.emit(NetworkManager.Events.Request, request);
+    if (this._requestIdToResponseEvent.has(event.requestId)) {
+      this._onResponseReceived(this._requestIdToResponseEvent.get(event.requestId));
+      this._onLoadingFinished({ requestId: event.requestId });
+      this._requestIdToResponseEvent.delete(event.requestId);
+    }
   }
 
 
   /**
@@ -226,16 +235,32 @@
     this.emit(NetworkManager.Events.Response, response);
     this.emit(NetworkManager.Events.RequestFinished, request);
   }
 
+  _onDataReceived(event) {
+    const request = this._requestIdToRequest.get(event.requestId);
+    if (!request || !request._response)
+      return;
+    request._response._size += event.dataLength;
+    if (event.encodedDataLength > 0) {
+      request._response._sizeEncoded += event.encodedDataLength;
+    }
+    this.emit(NetworkManager.Events.DataReceived, request._response);
+  }
+
   /**
    * @param {!Protocol.Network.responseReceivedPayload} event
    */
   _onResponseReceived(event) {
     const request = this._requestIdToRequest.get(event.requestId);
     // FileUpload sends a response without a matching request.
-    if (!request)
+    if (!request) {
+      if (event.response.url.indexOf('http') === 0) {
+        this._requestIdToResponseEvent.set(event.requestId, event);
+        this.emit(NetworkManager.Events.RequestIdle, { requestId: event.requestId, url: event.response.url });
+      }
       return;
+    }
     const response = new Response(this._client, request, event.response);
     request._response = response;
     this.emit(NetworkManager.Events.Response, response);
   }
@@ -248,9 +273,12 @@
     // For certain requestIds we never receive requestWillBeSent event.
     // @see https://crbug.com/750469
     if (!request)
       return;
-    request.response()._bodyLoadedPromiseFulfill.call(null);
+    const response = request.response();
+    if (!response)
+      return;
+    response._bodyLoadedPromiseFulfill.call(null);
     this._requestIdToRequest.delete(request._requestId);
     this._attemptedAuthentications.delete(request._interceptionId);
     this.emit(NetworkManager.Events.RequestFinished, request);
   }
@@ -299,8 +327,9 @@
     this._postData = event.request.postData;
     this._headers = {};
     this._frame = frame;
     this._redirectChain = redirectChain;
+    this._credentials = null;
     for (const key of Object.keys(event.request.headers))
       this._headers[key.toLowerCase()] = event.request.headers[key];
 
     this._fromMemoryCache = false;
@@ -379,8 +408,12 @@
       errorText: this._failureText
     };
   }
 
+  authenticate(credentials) {
+    this._credentials = credentials;
+  }
+
   /**
    * @param {!Object=} overrides
    */
   async continue(overrides = {}) {
@@ -510,8 +543,10 @@
     this._statusText = responsePayload.statusText;
     this._url = request.url();
     this._fromDiskCache = !!responsePayload.fromDiskCache;
     this._fromServiceWorker = !!responsePayload.fromServiceWorker;
+    this._size = 0;
+    this._sizeEncoded = 0;
     this._headers = {};
     for (const key of Object.keys(responsePayload.headers))
       this._headers[key.toLowerCase()] = responsePayload.headers[key];
     this._securityDetails = responsePayload.securityDetails ? new SecurityDetails(responsePayload.securityDetails) : null;
@@ -582,8 +617,16 @@
     }
     return this._contentPromise;
   }
 
+  size() {
+    return this._size;
+  }
+
+  sizeEncoded() {
+    return this._sizeEncoded;
+  }
+
   /**
    * @return {!Promise<string>}
    */
   async text() {
@@ -621,42 +664,8 @@
   }
 }
 helper.tracePublicAPI(Response);
 
-/**
- * @param {!Protocol.Network.Request} request
- * @return {string}
- */
-function generateRequestHash(request) {
-  let normalizedURL = request.url;
-  try {
-    // Decoding is necessary to normalize URLs. @see crbug.com/759388
-    // The method will throw if the URL is malformed. In this case,
-    // consider URL to be normalized as-is.
-    normalizedURL = decodeURI(request.url);
-  } catch (e) {
-  }
-  const hash = {
-    url: normalizedURL,
-    method: request.method,
-    postData: request.postData,
-    headers: {},
-  };
-
-  if (!normalizedURL.startsWith('data:')) {
-    const headers = Object.keys(request.headers);
-    headers.sort();
-    for (let header of headers) {
-      const headerValue = request.headers[header];
-      header = header.toLowerCase();
-      if (header === 'accept' || header === 'referer' || header === 'x-devtools-emulate-network-conditions-client-id' || header === 'cookie')
-        continue;
-      hash.headers[header] = headerValue;
-    }
-  }
-  return JSON.stringify(hash);
-}
-
 class SecurityDetails {
   /**
    * @param {!Protocol.Network.SecurityDetails} securityPayload
    */
@@ -706,10 +715,12 @@
 
 NetworkManager.Events = {
   Request: 'request',
   Response: 'response',
+  RequestIdle: 'requestidle',
   RequestFailed: 'requestfailed',
   RequestFinished: 'requestfinished',
+  DataReceived: 'datareceived',
 };
 
 const statusTexts = {
   '100': 'Continue',
