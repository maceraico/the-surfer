const fs = require('fs');
const path = require('path');
const JsDiff = require('diff');

let libDir = require.resolve('puppeteer-core');
libDir = libDir.substring(0, libDir.indexOf(`${path.sep}puppeteer-core${path.sep}`));
let patchesDir = path.resolve(`${__dirname}${path.sep}..${path.sep}patches`);

if (!path.isAbsolute(libDir)) {
  libDir = path.resolve(process.cwd(), libDir);
}
if (!path.isAbsolute(patchesDir)) {
  patchesDir = path.resolve(process.cwd(), patchesDir);
}

const getAllFiles = dir => fs.readdirSync(dir).reduce((files, file) => {
  const name = path.join(dir, file);
  const isDirectory = fs.statSync(name).isDirectory();
  return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
}, []);

getAllFiles(patchesDir).forEach((file) => {
  const filename = path.basename(file);
  let relativePath = path.dirname(file).replace(patchesDir, '');
  if (relativePath.indexOf('/') === 0) {
    relativePath = relativePath.substring(1);
  }

  const libraryFile = path.join(libDir, relativePath, filename);
  const patchFile = path.join(patchesDir, relativePath, filename);
  if (!fs.statSync(libraryFile).isFile(libraryFile)) {
    console.log(`${libraryFile} NOT exist, patch will be not applied.`);
    return;
  }
  if (!fs.statSync(patchFile).isFile(patchFile)) {
    console.log(`${patchFile} NOT exist, patch will be not applied.`);
    return;
  }

  const patchedContent = JsDiff.applyPatch(fs.readFileSync(libraryFile, 'utf-8'), fs.readFileSync(patchFile, 'utf-8'));
  if (patchedContent) {
    fs.writeFileSync(libraryFile, patchedContent, (error) => { throw error; });
  }
});

