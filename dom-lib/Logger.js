(function () {
  if (typeof window._TSE_ === 'undefined') {
    window._TSE_ = {};
  }
  function _exposedTSEListener() {
    window._TSE_.debug = window._TSE_logDebug;
    window._TSE_.info = window._TSE_logInfo;
    window._TSE_.success = window._TSE_logSuccess;
    window._TSE_.warning = window._TSE_logWarning;
    window._TSE_.error = window._TSE_logError;
  }
  setTimeout(_exposedTSEListener, 1);
}());
