(function () {
  if (typeof window._TSE_ === 'undefined') {
    window._TSE_ = {};
  }

  function Monitor() {
    const self = this;

    const TRESHOLDS = {
      maxTimeoutMS: 2000,
      maxIntervalMS: 10000,
    };

    const timeouts = [];

    const intervals = [];

    const queuedTimings = [];

    const evaluate = window.eval; // eslint-disable-line no-eval

    let isReady = false;

    let isMainContext = false;

    let resourcesObserver = null;

    let isWritingNewDocument = false;

    let inputFileTouched = null;

    let resourceTimingProcessed = _collectResourceTiming;

    function _init() {
      isMainContext = window.parent === window && !window.opener;
      setTimeout(_exposedTSEListener, 1);
      Document.prototype._TSE_querySelector = Document.prototype.querySelector;
      Document.prototype._TSE_querySelectorAll = Document.prototype.querySelectorAll;
      Document.prototype._TSE_appendChild = Document.prototype.appendChild;
      Element.prototype._TSE_querySelector = Element.prototype.querySelector;
      Element.prototype._TSE_querySelectorAll = Element.prototype.querySelectorAll;
      Element.prototype._TSE_appendChild = Element.prototype.appendChild;
      window._TSE_eval = window.eval; // eslint-disable-line no-eval
      window._TSE_setTimeout = window.setTimeout;
      Object.defineProperty(_setTimeout, 'toString', { get: () => (() => window._TSE_setTimeout.toString()) });
      window.setTimeout = _setTimeout;
      window._TSE_clearTimeout = window.clearTimeout;
      Object.defineProperty(_clearTimeout, 'toString', { get: () => (() => window._TSE_clearTimeout.toString()) });
      window.clearTimeout = _clearTimeout;
      window._TSE_setInterval = window.setInterval;
      Object.defineProperty(_setInterval, 'toString', { get: () => (() => window._TSE_setInterval.toString()) });
      window.setInterval = _setInterval;
      window._TSE_clearInterval = window.clearInterval;
      Object.defineProperty(_clearInterval, 'toString', { get: () => (() => window._TSE_clearInterval.toString()) });
      window.clearInterval = _clearInterval;
      document._TSE_open = document.open;
      document.open = function () {
        isWritingNewDocument = true;
        return document._TSE_open.apply(this, arguments); // eslint-disable-line prefer-rest-params
      };
      Object.defineProperty(document.open, 'toString', { get: () => (() => document._TSE_open.toString()) });
      document._TSE_close = document.close;
      document.close = function () {
        if (isWritingNewDocument) {
          _addListeners();
          isWritingNewDocument = false;
        }
        return document._TSE_close.apply(this, arguments); // eslint-disable-line prefer-rest-params
      };
      Object.defineProperty(document.close, 'toString', { get: () => (() => document._TSE_close.toString()) });
      _addListeners();
      resourcesObserver = new PerformanceObserver((list) => {
        const entries = list.getEntries();
        const entriesCount = entries.length;
        for (let i = 0; i < entriesCount; i++) {
          const entry = entries[i].toJSON();
          resourceTimingProcessed(entry, window.performance.timing.navigationStart, (window._TSE_.frameSpecs || { id: '' }).id, isMainContext);
        }
      });
      resourcesObserver.observe({ entryTypes: ['navigation', 'resource'] });
      return self;
    }

    function _exposedTSEListener() {
      if (isMainContext) {
        _flushTimingsQueue();
        isReady = true;
      }
      window._TSE_.internalPageCreation = window._TSE_internalPageCreation;
      window._TSE_.silentOpen = async function (...args) {
        await window._TSE_.internalPageCreation(true);
        const newWindow = window.open(...args);
        await new Promise((resolve) => {
          setTimeout(() => { resolve(); }, 100);
        });
        await window._TSE_.internalPageCreation(false);
        return newWindow;
      };
    }

    function _collectResourceTiming(entry) {
      queuedTimings.push(entry);
    }

    function _flushTimingsQueue() {
      // window._TSE_logInfo('_flushTimingsQueue:',  window._TSE_.frameSpecs.id);
      while (queuedTimings.length > 0) {
        const entry = queuedTimings.shift();
        window._TSE_resourceTimingProcessed(entry, window.performance.timing.navigationStart, window._TSE_.frameSpecs.id, isMainContext); // eslint-disable-line max-len
      }
      resourceTimingProcessed = window._TSE_resourceTimingProcessed;
    }

    function _addListeners() {
      _removeListeners();
      window.addEventListener('load', _windowLoadListener, false);
      window.addEventListener('click', _fileUploadListener, false);
      window.addEventListener('change', _fileUploadListener, false);
    }

    function _removeListeners() {
      window.removeEventListener('load', _windowLoadListener, false);
      window.removeEventListener('click', _fileUploadListener, false);
      window.removeEventListener('change', _fileUploadListener, false);
    }

    async function _windowLoadListener() {
      if (typeof window._TSE_onLoadScripts === 'undefined') {
        return;
      }
      let scripts = [];
      scripts = await window._TSE_onLoadScripts().catch(() => { scripts = []; });
      const keys = Object.keys(scripts);
      const keysCount = keys.length;
      for (let i = 0; i < keysCount; i++) {
        const key = keys[i];
        evaluate(scripts[key]);
      }
      // window._TSE_logInfo(`window loaded${window._TSE_ && window._TSE_.frameSpecs ? ` ${window._TSE_.frameSpecs.id}` : ''}`);
    }

    function _fileUploadListener(evt) {
      if (typeof window._TSE_emitFileUpload === 'undefined') {
        return;
      }
      const { target } = evt;
      if (!target || !(target instanceof HTMLElement)) {
        return;
      }
      const nodeName = (target.nodeName || '').toLowerCase();
      const nodeType = (target.type || '').toLowerCase();
      if (nodeName !== 'input' || nodeType !== 'file') {
        return;
      }
      inputFileTouched = target;
      window._TSE_emitFileUpload((window._TSE_.frameSpecs || { id: '' }).id, isMainContext);
    }

    function _setTimeout() {
      const args = Array.prototype.slice.call(arguments); // eslint-disable-line prefer-rest-params
      const ms = typeof args[1] !== 'undefined' ? parseInt(args[1], 10) : 0;
      if (ms <= TRESHOLDS.maxTimeoutMS) {
        let id = null;
        const orFunc = args[0];
        const extraArgs = args.slice(2);
        const func = function () {
          const index = timeouts.indexOf(id);
          if (typeof orFunc === 'function') {
            orFunc.apply(null, extraArgs); // eslint-disable-line prefer-spread
          } else if (typeof orFunc === 'string') {
            window._TSE_eval.apply(null, [orFunc]);
          }
          timeouts.splice(index, 1);
          // window._TSE_logDebug('timeout removed:', id, timeouts.length);
        };
        id = window._TSE_setTimeout.apply(null, [func].concat(args.slice(1)));
        timeouts.push(id);
        // window._TSE_logDebug('timeout added:', id, timeouts.length);
        return id;
      }
      return window._TSE_setTimeout.apply(null, args);
    }

    function _clearTimeout() {
      const args = Array.prototype.slice.call(arguments); // eslint-disable-line prefer-rest-params
      const id = args[0];
      const index = typeof id !== 'undefined' ? timeouts.indexOf(id) : -1;
      if (index > -1) {
        timeouts.splice(index, 1);
        // window._TSE_logDebug('timeout cleared:', id, timeouts.length);
      }
      return window._TSE_clearTimeout.apply(null, args);
    }

    function _setInterval() {
      let id = null;
      const args = Array.prototype.slice.call(arguments); // eslint-disable-line prefer-rest-params
      const ms = typeof args[1] !== 'undefined' ? parseInt(args[1], 10) : 0;
      const orFunc = args[0];
      const extraArgs = args.slice(2);
      const func = function () {
        const index = intervals.indexOf(id);
        if (typeof orFunc === 'function') {
          orFunc.apply(null, extraArgs); // eslint-disable-line prefer-spread
        } else if (typeof orFunc === 'string') {
          window._TSE_eval.apply(null, [orFunc]);
        }
        if (ms >= TRESHOLDS.maxIntervalMS) {
          intervals.splice(index, 1);
        }
        // window._TSE_logDebug('interval removed:', id, intervals.length);
      };
      id = window._TSE_setInterval.apply(null, [func].concat(args.slice(1)));
      intervals.push(id);
      // window._TSE_logDebug('interval added:', id, intervals.length);
      return id;
    }

    function _clearInterval() {
      const args = Array.prototype.slice.call(arguments); // eslint-disable-line prefer-rest-params
      const id = args[0];
      const index = typeof id !== 'undefined' ? intervals.indexOf(id) : -1;
      if (index > -1) {
        intervals.splice(index, 1);
        // window._TSE_logDebug('interval cleared:', id, intervals.length);
      }
      return window._TSE_clearInterval.apply(null, args);
    }

    self.counts = () => ({ timeouts: timeouts.length, intervals: intervals.length });

    self.inputFileTouched = () => {
      const elm = inputFileTouched;
      inputFileTouched = null;
      return elm;
    };

    self.triggerReady = () => {
      if (isReady === false) {
        _flushTimingsQueue();
        isReady = true;
      }
    };

    return _init();
  }

  window._TSE_.Monitor = new Monitor();
}());
