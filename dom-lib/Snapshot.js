(function () {
  if (typeof window._TSE_ === 'undefined') {
    window._TSE_ = {};
  }

  function Snapshot() {
    const self = this;

    const defaults = {
      nastedElmsCounts: { form: 0, a: 0 },
      replacedNodeName: 'tse-node-replaced',
      replacedAttrName: 'tse-nodename',
      dataUrlAttrName: 'tse-data-url',
      internalScript: true,
      scriptsToKeepSelector: null,
    };

    let scriptsToKeep = [];

    let nastedElmsCounts = null;

    let { replacedNodeName } = defaults;

    let { replacedAttrName } = defaults;

    let { dataUrlAttrName } = defaults;

    let { scriptsToKeepSelector } = defaults;

    function _init() {
      window._TSE_setTimeout(_exposedTSEListener, 1);
      return self;
    }

    function _exposedTSEListener() {
      window._TSE_.snapshotConf = window._TSE_snapshotConf;
    }

    /* eslint-disable */
    function _internalScript() {
      var elmsToReplace = document._TSE_querySelectorAll('head > script[type="text/' + replacedNodeName + '"]');
      var elmsToReplaceCount = elmsToReplace.length;
      for (var i = elmsToReplaceCount - 1; i >= 0; i--) {
        var elm = elmsToReplace[i];
        var tmpDiv = document.createElement('div');
        tmpDiv.innerHTML = decodeURIComponent(elm.innerHTML);
        elm.parentNode.replaceChild(tmpDiv.firstChild, elm);
      }
      elmsToReplace = document.getElementsByTagName(replacedNodeName);
      elmsToReplaceCount = elmsToReplace.length;
      for (var i = elmsToReplaceCount - 1; i >= 0; i--) {
        var elm = elmsToReplace[i];
        var attrs = elm.attributes;
        var attrsCount = attrs.length;
        var realNode = document.createElement(elm.getAttribute(replacedAttrName));
        for (var j = 0; j < attrsCount; j++) {
          try {
            realNode.setAttribute(attrs[j].name, attrs[j].value);
          } catch (e) {
            // continue regardless of error
          }
        }
        realNode.removeAttribute(replacedAttrName);
        while (elm.firstChild) {
          realNode._TSE_appendChild(elm.firstChild);
        }
        elm.parentNode.replaceChild(realNode, elm);
      }
      var canvas = document._TSE_querySelectorAll('canvas[' + dataUrlAttrName + ']');
      var canvasCount = canvas.length;
      for (var i = 0; i < canvasCount; i++) {
        var elm = canvas[i];
        var img = new Image();
        var ctx = elm.getContext('2d');
        img.onload = () => { ctx.drawImage(img, 0, 0); };
        img.src = elm.getAttribute(dataUrlAttrName);
      }
    }
    /* eslint-enable */

    function _getChildNodes(elm) {
      return elm.shadowRoot ? elm.shadowRoot.childNodes : elm.childNodes;
    }

    function _importElement(elm, parentElm) {
      if (!elm) {
        return;
      }

      let nodeNameAttr = null;
      let nodeName = (elm.nodeName || '').toLowerCase();
      if (!nodeName || ((nodeName === 'script' || nodeName === 'noscript') && scriptsToKeep.indexOf(elm) === -1)) {
        return;
      }

      const tmpDOM = parentElm.ownerDocument;
      let isTrgtNastedElm = false;
      let clonedElm = null;
      if (nodeName === '#text') {
        clonedElm = tmpDOM.createTextNode(elm.nodeValue);
      } else if (nodeName === '#comment') {
        clonedElm = tmpDOM.createComment(elm.nodeValue);
      } else {
        // Check if we need to rename the node
        let parentNodeName = (parentElm.nodeName || '').toLowerCase();
        if (parentNodeName === replacedNodeName) {
          parentNodeName = parentElm.getAttribute(replacedAttrName);
        }
        isTrgtNastedElm = typeof nastedElmsCounts[nodeName] !== 'undefined';
        if (isTrgtNastedElm) {
          if (nastedElmsCounts[nodeName] > 0) {
            nodeNameAttr = nodeName;
            nodeName = replacedNodeName;
          }
          nastedElmsCounts[nodeName]++;
        } else if (nodeName === 'li' && parentNodeName === 'li') {
          nodeNameAttr = nodeName;
          nodeName = replacedNodeName;
        } else if (parentNodeName === 'head' && (nodeName !== 'title' && nodeName !== 'base' && nodeName !== 'link' && nodeName !== 'meta' && nodeName !== 'script' && nodeName !== 'style')) {
          nodeNameAttr = nodeName;
          nodeName = replacedNodeName;
        }
        // Try-Catch is needed because sometimes nodeName could be not valid
        try {
          clonedElm = tmpDOM.createElement(nodeName);
        } catch (e) {
          try {
            // Trying a workaround for invalid nodeName
            const tmpElm = elm.ownerDocument.createElement('div');
            tmpElm.innerHTML = `<${nodeName}></${nodeName}>`;
            if (tmpElm.firstChild) {
              clonedElm = tmpElm.firstChild;
            }
          } catch (e) { // eslint-disable-line no-shadow
            clonedElm = null;
          }
        }

        if (clonedElm) {
          // Setting nodename attribute if the node has been renamed
          if (nodeNameAttr) {
            clonedElm.setAttribute(replacedAttrName, nodeNameAttr);
          }

          // Importing attributes
          const attrs = elm.attributes;
          const attrsCount = attrs.length;
          for (let i = 0; i < attrsCount; i++) {
            // Try-Catch is needed because sometimes attrs[i].name could be NOT valid
            try {
              if ((attrs[i].name || '').indexOf('on') !== 0 && typeof elm[attrs[i].name] !== 'function') {
                clonedElm.setAttribute(attrs[i].name, attrs[i].value);
              }
            } catch (e) {
              // continue regardless of error
            }
          }
          if (nodeName === 'script') {
            clonedElm.setAttribute('type', 'text/tse-script-to-keep');
          }
        }
      }

      parentElm._TSE_appendChild(clonedElm);

      if (nodeName === 'style' && elm.textContent.trim() === '' && elm.sheet && elm.sheet.rules && elm.sheet.rules.length) {
        const textRules = [];
        const rulesCount = elm.sheet.rules.length;
        for (let i = 0; i < rulesCount; i++) {
          const cssRule = elm.sheet.rules[i];
          if (cssRule.cssText) {
            textRules.push(cssRule.cssText);
          }
        }
        if (textRules.length) {
          clonedElm.innerHTML = textRules.join('\n');
        }
      } else {
        const children = _getChildNodes(elm);
        const childrenCount = children.length;
        for (let i = 0; i < childrenCount; i++) {
          _importElement(children[i], clonedElm);
        }
      }

      if (isTrgtNastedElm) {
        nastedElmsCounts[nodeName]--;
      }
    }

    this.html = async function () {
      // Return empty DOM (if needed)
      if (!document || !document.documentElement) {
        return '<html><head></head><body></body></html>';
      }

      // Importing configuration from node context or set defaults
      let conf = null;
      if (typeof window._TSE_snapshotConf === 'function') {
        try {
          conf = await window._TSE_snapshotConf().catch(() => {
            conf = null;
          });
        } catch (e) {
          conf = null;
        }
      }
      if (!conf) {
        conf = JSON.parse(JSON.stringify(defaults));
      }
      ({ replacedNodeName } = conf);
      ({ replacedAttrName } = conf);
      ({ dataUrlAttrName } = conf);
      ({ scriptsToKeepSelector } = conf);
      scriptsToKeep = [];
      if (scriptsToKeepSelector) {
        try {
          scriptsToKeep = Array.prototype.slice.call(document.querySelectorAll(scriptsToKeepSelector));
        } catch (error) {
          scriptsToKeep = [];
        }
      }
      nastedElmsCounts = JSON.parse(JSON.stringify(defaults.nastedElmsCounts));

      // Creating tmpDOM
      const tmpDOM = document.implementation.createHTMLDocument('');
      tmpDOM.removeChild(tmpDOM.documentElement);
      tmpDOM._TSE_appendChild(tmpDOM.importNode(document.documentElement, false));

      // Importing children of document.documentElement in tmpDOM
      const children = document.documentElement.childNodes;
      const childrenCount = children.length;
      const importInternalScript = conf.internalScript;
      for (let i = 0; i < childrenCount; i++) {
        _importElement(children[i], tmpDOM.documentElement);
      }

      // Importing internalScript (if needed)
      if (importInternalScript) {
        const internalScript = tmpDOM.createElement('script');
        internalScript.innerHTML = `(function(){
  var replacedNodeName = '${replacedNodeName}';
  var replacedAttrName = '${replacedAttrName}';
  var dataUrlAttrName = '${dataUrlAttrName}';
  ${_internalScript.toString()}
  _internalScript();
})()`;
        tmpDOM.body._TSE_appendChild(internalScript);
      }

      // Setting base element
      const port = window.location.port ? `:${window.location.port}` : '';
      let basePath = window.location.pathname;
      if (basePath.indexOf('/') === 0) {
        basePath = basePath.substring(1);
      }
      const lastSlashIndex = basePath.lastIndexOf('/');
      if (lastSlashIndex < basePath.length - 1) {
        basePath = basePath.substring(0, lastSlashIndex);
      }
      if (basePath !== '') {
        basePath += '/';
      }
      let baseUrl = `${window.location.protocol}//${window.location.hostname}${port}/${basePath}`;
      const realBaseElm = document.head._TSE_querySelector('base');
      const fakeBaseElm = realBaseElm ? tmpDOM.head._TSE_querySelector('base') : tmpDOM.createElement('base');
      if (realBaseElm && realBaseElm.getAttribute('href') !== null) {
        baseUrl = realBaseElm.href;
      }
      fakeBaseElm.removeAttribute('target');
      fakeBaseElm.setAttribute('href', baseUrl);
      tmpDOM.head.insertBefore(fakeBaseElm, tmpDOM.head.firstChild);

      // Converting replaced nodes in scripts if they are a descendant of head
      const trgtHeadChild = tmpDOM._TSE_querySelectorAll('head > tse-node-replaced');
      const trgtHeadChildCount = trgtHeadChild.length;
      for (let i = trgtHeadChildCount - 1; i >= 0; i--) {
        const scriptElm = tmpDOM.createElement('script');
        scriptElm.setAttribute('type', 'text/tse-node-replaced');
        scriptElm.innerHTML = encodeURIComponent(trgtHeadChild[i].outerHTML);
        tmpDOM.head.replaceChild(scriptElm, trgtHeadChild[i]);
      }

      // Setting dataUrl for canvas
      const realCanvas = document._TSE_querySelectorAll('canvas');
      const fakeCanvas = tmpDOM._TSE_querySelectorAll('canvas');
      const canvasCount = realCanvas.length;
      for (let i = 0; i < canvasCount; i++) {
        // Try catch is needed because toDataURL() can trigger a security exception
        try {
          const dataUrl = realCanvas[i].toDataURL();
          fakeCanvas[i].setAttribute(dataUrlAttrName, dataUrl);
        } catch (e) {
          // continue regardless of error
        }
      }

      // Setting proper values for inputs
      const realInputs = document._TSE_querySelectorAll('input, select, textarea');
      const fakeInputs = tmpDOM._TSE_querySelectorAll('input, select, textarea');
      const inputsCount = realInputs.length;
      for (let i = 0; i < inputsCount; i++) {
        const realInput = realInputs[i];
        const fakeInput = fakeInputs[i];
        const nodeName = realInput.nodeName.toLowerCase();
        if (nodeName === 'input') {
          const inputType = realInput.type ? realInput.type.toLowerCase() : '';
          if (inputType === 'radio' || inputType === 'checkbox') {
            if (realInput.checked) {
              fakeInput.setAttribute('checked', 'checked');
            } else {
              fakeInput.removeAttribute('checked');
            }
          } else {
            fakeInput.setAttribute('value', realInput.value);
          }
        } else if (nodeName === 'select') {
          const realOptions = realInput._TSE_querySelectorAll('option');
          const fakeOptions = fakeInput._TSE_querySelectorAll('option');
          const optionsCount = realInput.length;
          for (let j = 0; j < optionsCount; j++) {
            if (realOptions[j].selected) {
              fakeOptions[j].setAttribute('selected', 'selected');
            } else {
              fakeOptions[j].removeAttribute('selected');
            }
          }
        } else if (nodeName === 'textarea') {
          fakeInput.innerHTML = realInput.value;
        }
      }

      // Calculating !DOCTYPE
      const docType = document.doctype;
      let docTypeHTML = '';
      if (docType && docType.name) {
        docTypeHTML = `<!DOCTYPE ${docType.name}${docType.publicId ? ` PUBLIC "${docType.publicId}"` : ''}${!docType.publicId && docType.systemId ? ' SYSTEM' : ''}${docType.systemId ? ` "${docType.systemId}"` : ''}>`;
      }

      return docTypeHTML + tmpDOM.documentElement.outerHTML;
    };

    return _init();
  }

  window._TSE_.Snapshot = new Snapshot();
}());
