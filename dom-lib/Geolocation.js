(function () {
  function Geolocation() {
    const self = this;

    const defaultPostion = {
      coords: {
        accuracy: 20,
        altitude: null,
        altitudeAccuracy: null,
        heading: null,
        latitude: null,
        longitude: null,
        speed: null,
      },
      timestamp: null,
    };

    const watched = [];

    const deleted = [];

    this.getCurrentPosition = async function (successCallback, errorCallback) {
      if (typeof successCallback === 'undefined') {
        throw new TypeError('Not enough arguments');
      }
      if (typeof successCallback !== 'function') {
        throw new TypeError('Argument 1 (\'successCallback\') to Geolocation.getCurrentPosition must be a function');
      }
      window._TSE_getGeolocation().then((status) => {
        if (window.location.protocol !== 'https:') {
          if (typeof errorCallback === 'function') {
            errorCallback({
              code: 2,
              message: 'Origin does not have permission to use Geolocation service',
            });
            return;
          }
          throw new Error(`[blocked] Access to geolocation was blocked over insecure connection to ${window.location.protocol}//${window.location.hostname}`);
        }
        if (status.enabled === false) {
          if (typeof errorCallback === 'function') {
            errorCallback({
              code: 1,
              message: 'User denied Geolocation',
            });
          }
          return;
        }
        const position = JSON.parse(JSON.stringify(defaultPostion));
        position.coords.accuracy = status.accuracy;
        position.coords.latitude = status.latitude;
        position.coords.longitude = status.longitude;
        position.timestamp = new Date().getTime();
        successCallback(position);
      });
    };

    this.watchPosition = function (successCallback, errorCallback) {
      if (typeof successCallback === 'undefined') {
        throw new TypeError('Not enough arguments');
      }
      if (typeof successCallback !== 'function') {
        throw new TypeError('Argument 1 (\'successCallback\') to Geolocation.watchPosition must be a function');
      }
      const watchedObj = { success: successCallback, error: errorCallback };
      let id = null;
      if (deleted.length) {
        id = deleted.pop();
        watched[id] = watchedObj;
      } else {
        watched.push(watchedObj);
        id = watched.length - 1;
      }
      self.getCurrentPosition(successCallback, errorCallback);
      return id + 1;
    };

    this.clearWatch = function (_id) {
      let id = parseInt(_id, 10);
      if (Number.isNaN(id)) {
        return;
      }
      id--;
      if (id < 0 || id > watched.length - 1) {
        return;
      }
      if (id === watched.length - 1) {
        watched.pop();
        return;
      }
      watched[id] = null;
      if (deleted.indexOf(id) === -1) {
        deleted.push(id);
      }
    };

    this._TSE_positionUpdated = function () {
      const watchedObjsCount = watched.length;
      for (let i = 0; i < watchedObjsCount; i++) {
        const watchedObj = watched[i];
        if (typeof watchedObj.success === 'function') {
          self.getCurrentPosition(watchedObj.success, watchedObj.error);
        }
      }
    };
  }

  Object.defineProperty(navigator, 'geolocation', { writable: true });
  navigator.geolocation = new Geolocation();
  Object.defineProperty(navigator, 'geolocation', { writable: false });
}());
