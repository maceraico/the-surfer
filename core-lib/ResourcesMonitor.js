const { inherits } = require('util');
const EventEmitter = require('events');
const logger = require('./Logger.js').client('ResourcesMonitor');

/**
 * ResourcesMonitor class is used by WorkspaceWatcher in order to understand if a page is successfully loaded
 * by observing the resources requested through the network.
 * When the monitor is running, it will check the requests stack everytime an amount of time is passed,
 * when the stack will be empty the loaded event will be triggered.
 * Moreover WorkspaceWatcher will propagate network-activity events.
 * @constructor
 * @param {Workspace} workspace
 * @param {Puppeter.Page} page
 * @emits ResourcesMonitor#loaded
 * @emits ResourcesMonitor#started
 * @emits ResourcesMonitor#stopped
 * @emits ResourcesMonitor#paused
 * @emits ResourcesMonitor#resumed
 * @emits ResourcesMonitor#network-activity-acknowledged
 * @emits ResourcesMonitor#network-activity-discarded
 * @emits ResourcesMonitor#network-activity-processed
 * @emits ResourcesMonitor#network-activity-ignored
 */
function ResourcesMonitor(_workspace, _page) {
  const self = this;

  const workspace = _workspace;

  const manager = workspace.resourcesManager();

  const page = _page;

  const mainFrame = page.mainFrame();

  const TRESHOLDS = {
    startDelay: 125,
    checkInterval: 500,
    unwatch: 30,
    ping: 30,
  };

  const timers = {
    starter: null,
    checker: null,
    watchdogs: {},
  };

  const idleRequests = {};

  const idleTimings = new WeakMap();

  let isRunning = false;

  let isPaused = false;

  let isLoaded = false;

  let isPageLoaded = false;

  let lockPageLoadedFlag = false;

  let postponePageLoadedEvent = false;

  let stack = [];

  let ignoredStack = [];

  let unwatchedResources = [];

  let receivedResources = new WeakMap();

  let processedResources = {};

  function _init() {
    workspace.addListener('page-destroyed', _pageDestroyedListener);
    workspace.addListener('resource-timing-processed', _resourceTimingListener);
    page.addListener('load', _pageLoadedListener);
    page.addListener('framenavigated', _frameNavigatedListener);
    page.addListener('framedetached', _frameDetachedListener);
    page.addListener('request', _addResource);
    page.addListener('requestidle', _addRequestIdle);
    page.addListener('requestfailed', _removeResource);
    page.addListener('response', _removeResource);
    self.stop();
    return self;
  }

  function _pageDestroyedListener(destroyedPage) {
    if (destroyedPage === page) {
      workspace.removeListener('page-destroyed', _pageDestroyedListener);
      workspace.removeListener('resource-timing-processed', _resourceTimingListener);
      page.removeListener('load', _pageLoadedListener);
      page.removeListener('framenavigated', _frameNavigatedListener);
      page.removeListener('framedetached', _frameDetachedListener);
      page.removeListener('request', _addResource);
      page.removeListener('requestidle', _addRequestIdle);
      page.removeListener('requestfailed', _removeResource);
      page.removeListener('response', _removeResource);
    }
  }

  function _resourceTimingListener(resourceTiming, frame, framePage) {
    if (framePage !== page) {
      return;
    }
    logger.debug('resourceTiming:', frame._id, resourceTiming, 'dev');
    const resourcesMap = receivedResources.has(frame) ? receivedResources.get(frame) : {};
    const targetStack = resourcesMap[resourceTiming.url];
    let resource = null;
    if (typeof targetStack !== 'undefined') {
      const resourcesCount = targetStack.length;
      if (resourcesCount) {
        let lastOffset = null;
        let resourceIndex = 0;
        if (resourcesCount > 1) {
          // Searching for a resource with consistent startTime
          for (let i = 0; i < resourcesCount; i++) {
            resource = targetStack[i];
            const offset = Math.abs(resource.timing().startTime - resourceTiming.startTime);
            if (lastOffset === null || offset < lastOffset) {
              resourceIndex = i;
              lastOffset = offset;
            }
          }
        }
        resource = targetStack.splice(resourceIndex, 1).pop();
        if (resourceIndex === 0) {
          delete resourcesMap[resourceTiming.url];
        }
      }
    }
    if (resource === null) {
      if (typeof idleRequests[resourceTiming.url] !== 'undefined') {
        if (!idleTimings.has(frame)) {
          idleTimings.set(frame, {});
        }
        const idleTimingsInFrame = idleTimings.get(frame);
        if (typeof idleTimingsInFrame[resourceTiming.url] === 'undefined') {
          idleTimingsInFrame[resourceTiming.url] = [];
        }
        idleTimingsInFrame[resourceTiming.url].push(resourceTiming);
      } else {
        logger.warning('resourceTiming ignored:', resourceTiming.url, 'dev');
      }
      return;
    }
    const redirectChain = resource.request().redirectChain();
    const redirectsCount = redirectChain.length;
    if (redirectsCount > 0 && !resource.isRedirect() && !resource.isError()) {
      const firstRequest = redirectChain[0];
      const lastRequest = redirectChain[redirectsCount - 1];
      const redirectStartTime = manager.resource(firstRequest).timing().startTime;
      const redirectEndTime = manager.resource(lastRequest).timing().endTime;
      const offset = redirectEndTime - redirectStartTime;
      Object.keys(resourceTiming).filter(key => key !== 'startTime').forEach((key) => {
        if (resourceTiming[key] > 0) {
          resourceTiming[key] -= offset;
        }
      });
      resourceTiming.startTime = redirectEndTime;
    }
    resource.timing(resourceTiming);
    self.emit('network-activity-processed', resource);
  }

  function _pageLoadedListener() {
    if (lockPageLoadedFlag === true) {
      postponePageLoadedEvent = true;
      return;
    }
    isPageLoaded = true;
    postponePageLoadedEvent = false;
    logger.debug('page is loaded');
  }

  async function _frameNavigatedListener(frame) {
    if (frame === mainFrame) {
      lockPageLoadedFlag = true;
      const requestsToAbort = { stack: stack.slice(), ignoredStack: ignoredStack.slice() };
      const loadedStatusBeforeEvaluation = isPageLoaded;
      let loadedStatus = null;
      loadedStatus = await frame.evaluate(() => typeof window._TSE_ !== 'undefined' && typeof window._TSE_.$ !== 'undefined').catch(() => {
        loadedStatus = false;
      });
      if (loadedStatus === false) {
        // When real navigation happens we need to remove from stacks the pending requests sent before the event
        const abortedRequests = [];
        requestsToAbort.stack.forEach((id) => {
          const index = stack.indexOf(id);
          if (index > -1) {
            stack.splice(index, 1);
            abortedRequests.push(id);
          }
        });
        requestsToAbort.ignoredStack.forEach((id) => {
          const index = stack.indexOf(id);
          if (index > -1) {
            stack.splice(index, 1);
            abortedRequests.push(id);
          }
        });
        logger.warning(`aborted requests per navigation: ${abortedRequests.join(', ')}`, 'dev');
      }
      if (!(loadedStatusBeforeEvaluation === false && isPageLoaded === true)) {
        // setting evaluated status only if page status is not changed during evaluation
        isPageLoaded = loadedStatus;
      }
      lockPageLoadedFlag = false;
      if (postponePageLoadedEvent === true) {
        _pageLoadedListener();
      }
    }
  }

  function _frameDetachedListener(frame) {
    if (receivedResources.has(frame)) {
      receivedResources.delete(frame);
    }
  }

  function _addRequestIdle(data) {
    if (typeof idleRequests[data.url] === 'undefined') {
      idleRequests[data.url] = [];
    }
    idleRequests[data.url].push(data.requestId);
  }

  function _addResource(request) {
    if (request._interceptionHandled === true) {
      return;
    }
    manager.setStartTime(request);
    const ignored = !isRunning;
    const urlToLog = (request.url() || '').startsWith('data:') ? 'data object' : request.url();
    const resource = manager.resource(request);
    let currStack = stack;
    let logsSuffix = '';
    let acknowledgedEventName = 'network-activity-acknowledged';
    let discardedEventName = 'network-activity-discarded';
    if (ignored) {
      currStack = ignoredStack;
      logsSuffix = 'Ignored';
      acknowledgedEventName = 'network-activity-ignored';
      discardedEventName = 'network-activity-ignored';
    }
    if (!resource.isBlacklisted()) {
      const targetStack = resource.isPing() ? unwatchedResources : currStack;
      targetStack.push(request._interceptionId);
      manager.process(resource);
      self.emit(acknowledgedEventName, resource);
      if (!resource.isPing()) {
        logger.debug(`addResource${logsSuffix}:`, urlToLog, 'AS', `#${request._interceptionId}`, 'verbose');
        timers.watchdogs[request._interceptionId] = _startWatchdog(request._interceptionId);
      } else {
        logger.debug(`addPingResource${logsSuffix}:`, urlToLog, 'AS', `#${request._interceptionId}`);
      }
    } else {
      manager.discard(resource);
      self.emit(discardedEventName, resource);
      logger.warning(`addResource${logsSuffix}:`, urlToLog, 'is blacklisted');
    }
  }

  function _removeResource(internalResource) {
    manager.setEndTime(internalResource);
    const ignored = !isRunning;
    const logsSuffix = ignored ? 'Ignored' : '';
    const resource = manager.resource(internalResource);
    const request = typeof internalResource.request === 'function' ? internalResource.request() : internalResource;
    const watchdog = timers.watchdogs[request._interceptionId];
    clearTimeout(watchdog);
    delete timers.watchdogs[request._interceptionId];
    let targetStack = !ignored ? stack : ignoredStack;
    let resourceIndex = targetStack.indexOf(request._interceptionId);
    if (resourceIndex === -1) {
      targetStack = unwatchedResources;
      resourceIndex = targetStack.indexOf(request._interceptionId);
    }
    if (resourceIndex > -1) {
      targetStack.splice(resourceIndex, 1);
      logger.debug(`removeResource${logsSuffix}:`, `#${request._interceptionId}`, `(${resource.status()})`, 'verbose');
      const url = resource.url();
      const frame = request.frame();
      if (!frame) {
        const idleIndex = typeof idleRequests[url] !== 'undefined' ? idleRequests[url].indexOf(request._requestId) : -1;
        if (idleIndex > -1) {
          idleRequests[url].splice(idleIndex, 1);
        }
        return;
      }
      if (!receivedResources.has(frame)) {
        receivedResources.set(frame, {});
      }
      const resourcesMap = receivedResources.get(frame);
      if (typeof resourcesMap[url] === 'undefined') {
        resourcesMap[url] = [];
      }
      resourcesMap[url].push(resource);
      const idleIndex = typeof idleRequests[url] !== 'undefined' ? idleRequests[url].indexOf(request._requestId) : -1;
      if (idleIndex > -1) {
        idleRequests[url].splice(idleIndex, 1);
        if (!idleRequests[url].length) {
          delete idleRequests[url];
        }
        const idleTimingsInFrame = idleTimings.get(frame) || {};
        if (typeof idleTimingsInFrame[url] !== 'undefined') {
          const timing = idleTimingsInFrame[url].shift();
          if (!idleTimingsInFrame[url].length) {
            delete idleTimingsInFrame[url];
            if (!Object.keys(idleTimingsInFrame).length) {
              idleTimings.delete(frame);
            }
          }
          logger.warning('manual trigger resourceTiming per idle:', url, 'dev');
          _resourceTimingListener(timing, frame, page);
        }
      } else if (resource.isRedirect() || resource.isError()) {
        const timing = resource.timing();
        timing.url = url;
        logger.warning(`manual trigger resourceTiming per ${resource.isRedirect() ? 'redirect' : 'error'}:`, url, 'dev');
        _resourceTimingListener(timing, frame, page);
      }
      const resKey = `${resource.method()}||${resource.url()}`;
      if (typeof processedResources[resKey] === 'undefined') {
        processedResources[resKey] = 0;
      }
      processedResources[resKey]++;
      if (processedResources[resKey] >= TRESHOLDS.ping) {
        manager.addPingRecognizer('urlEquals', resource.url(), resource.method());
        logger.warning('ping detected:', `#${request._interceptionId}`);
      }
    }
    if (request.isNavigationRequest() && !resource.isRedirect() && resource.isBinary()) {
      /**
       * Binary resources (except for XML) make frame impossible to load, so we need to trigger a redirect to about:blank
       */
      if (!resource.isXML()) {
        const frame = request.frame();
        logger.warning(`${request.url()} is binary, redirecting ${frame._id} to about:blank`, 'verbose');
        frame.evaluate(() => {
          window.location.href = 'about:blank';
        }).catch(() => {
          // continue
        });
      }
    }
  }

  function _checker() {
    if (lockPageLoadedFlag === true) {
      return;
    }
    if (!stack.length) {
      const pageHasDialog = workspace.dialog(page) !== null;
      if (isPageLoaded || pageHasDialog) {
        self.stop();
        isLoaded = true;
        logger.info('loaded');
        self.emit('loaded');
      } else {
        logger.warning('checker:', 'page not loaded yet');
      }
    } else {
      logger.debug('checker:', stack.length, 'verbose');
    }
  }

  function _startWatchdog(id) {
    return setTimeout(() => { _unwatchResource(id); }, TRESHOLDS.unwatch * 1000);
  }

  function _unwatchResource(id) {
    const resourceIndex = stack.indexOf(id);
    if (resourceIndex > -1) {
      stack.splice(resourceIndex, 1);
      unwatchedResources.push(id);
      logger.warning('unwatch per timeout:', `#${id}`);
    }
  }

  /**
   * Starts the monitoring, it means the monitor will observe the network activity.
   */
  this.start = (keepIgnored = false) => {
    if (isRunning) {
      logger.warning('start not possible, monitor is already started.', 'verbose');
      return false;
    }
    stack = keepIgnored === true ? ignoredStack.slice() : [];
    ignoredStack = [];
    unwatchedResources = [];
    receivedResources = new WeakMap();
    processedResources = {};
    isRunning = false;
    isPaused = false;
    isLoaded = false;
    logger.debug('started');
    timers.starter = setTimeout(() => {
      self.emit('started');
      timers.checker = setInterval(_checker, TRESHOLDS.checkInterval);
    }, TRESHOLDS.startDelay);
    isRunning = true;
    return true;
  };

  /**
   * Stops the monitoring, it means the monitor will ignore the network activity.
   */
  this.stop = () => {
    timers.starter = clearTimeout(timers.starter);
    timers.checker = clearInterval(timers.checker);
    Object.keys(timers.watchdogs).forEach((key) => {
      clearTimeout(timers.watchdogs[key]);
      delete timers.watchdogs[key];
    });
    if (!isRunning) {
      logger.warning('stop not possible, monitor is already stopped.', 'verbose');
      return false;
    }
    ignoredStack = stack.slice();
    stack = [];
    isRunning = false;
    isPaused = false;
    logger.debug('stopped');
    self.emit('stopped');
    return true;
  };

  /**
   * Pauses the monitoring, it means the monitor will continue to observe the network activity
   * but the trigger of the loaded event is prevented.
   */
  this.pause = () => {
    if (typeof timers.starter === 'undefined' && typeof timers.checker === 'undefined') {
      logger.warning('pause not possible, monitor is not active.', 'verbose');
      return false;
    }
    timers.starter = clearTimeout(timers.starter);
    timers.checker = clearInterval(timers.checker);
    isPaused = true;
    logger.debug('paused');
    self.emit('paused');
    return true;
  };

  /**
   * Resumes the monitoring, it means the trigger of the loaded event is not prevented anymore.
   */
  this.resume = () => {
    if (typeof timers.starter !== 'undefined' || typeof timers.checker !== 'undefined') {
      logger.warning('resume not possible, monitor is active.', 'verbose');
      return false;
    }
    timers.starter = setTimeout(() => {
      logger.debug('resumed');
      self.emit('resumed');
      timers.checker = setInterval(_checker, TRESHOLDS.checkInterval);
    }, TRESHOLDS.startDelay);
    isPaused = false;
    return true;
  };

  /**
   * Returns if the monitoring is in progress
   * @return {boolean}
   */
  this.isRunning = () => isRunning;

  /**
   * Returns if the monitoring is paused
   * @return {boolean}
   */
  this.isPaused = () => isPaused;

  /**
   * Returns if the monitoring is started and completed
   * @return {boolean}
   */
  this.isLoaded = () => isLoaded;

  /**
   * Defines and returns the current configuration
   * @param {ResourcesMonitorConf} [conf] If passed, the configuration will be updated.
   * @return {ResourcesMonitorConf}
   */
  this.configuration = (conf) => {
    if (typeof conf === 'object') {
      if (typeof conf.startDelay === 'number') {
        TRESHOLDS.startDelay = parseInt(conf.startDelay, 10);
      }
      if (typeof conf.checkInterval === 'number') {
        TRESHOLDS.checkInterval = parseInt(conf.checkInterval, 10);
      }
      if (typeof conf.unwatch === 'number') {
        TRESHOLDS.unwatch = parseInt(conf.unwatch, 10);
      }
      if (typeof conf.ping === 'number') {
        TRESHOLDS.ping = parseInt(conf.ping, 10);
      }
    }
    return {
      startDelay: TRESHOLDS.startDelay,
      checkInterval: TRESHOLDS.checkInterval,
      unwatch: TRESHOLDS.unwatch,
      ping: TRESHOLDS.ping,
    };
  };

  return _init();
}

inherits(ResourcesMonitor, EventEmitter);

/**
 * This event is emitted when the monitoring is started and completed properly,
 * it means there are no resources in requests stack.
 * Mainly this event is listen by WorkspaceWatcher in order to run the next monitor in the chain.
 * @event ResourcesMonitor#loaded
 */

/**
 * This event is emitted when the monitoring is started.
 * @event ResourcesMonitor#started
 */

/**
 * This event is emitted when the monitoring is stopped.
 * @event ResourcesMonitor#stopped
 */

/**
 * This event is emitted when the monitoring is paused.
 * @event ResourcesMonitor#paused
 */

/**
 * This event is emitted when the monitoring is resumed.
 * @event ResourcesMonitor#resumed
 */

/**
 * This event is emitted when a resource has been intercepted, customized (if needed) and allowed to be requested.
 * @event ResourcesMonitor#network-activity-acknowledged
 * @param {Resource}
 */

/**
 * This event is emitted when a resource has been intercepted and aborted.
 * @event ResourcesMonitor#network-activity-discarded
 * @param {Resource}
 */

/**
 * This event is emitted when a resource has been requested and downloaded.
 * @event ResourcesMonitor#network-activity-processed
 * @param {Resource}
 */

/**
 * This event is emitted when the monitoring is stopped and a resource has been intercepted, customized (if needed)
 * and allowed to be requested.
 * Mainly this event is listen by WorkspaceWatcher in order to restart the monitoring process if the chain is not empty.
 * @event ResourcesMonitor#network-activity-ignored
 * @param {Resource}
 */

/**
 * @typedef {Object} ResourcesMonitorConf
 * @property {number} [startDelay] The time, in milliseconds, the monitoring should be delayed before start. Defaults to 125.
 * @property {number} [checkInterval] The amount of time, in milliseconds, to pass between each execution of the loaded checker. Defaults to 500.
 * @property {number} [unwatch] The amount of times before a request is not considered anymore by monitor (timeout). Defaults to 30.
 * @property {number} [ping] The amount of times an "equal" resource can be requested before to consider it as ping. Defaults to 30.
 */

module.exports = ResourcesMonitor;
