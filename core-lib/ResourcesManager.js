const NetworkUtils = require('./NetworkUtils.js');
const Resource = require('./Resource.js');

/**
 * ResourcesManager class is used by ResourcesMonitor in order to generate the resources.
 * It provides a set of methods in order to recognize, customize and block the requests that will be performed.
 * @constructor
 */
function ResourcesManager() {
  const self = this;

  const reservedHeaderNames = ['host', 'cookie'];

  const rules = {
    recognizers: [],
    blacklist: [],
    headers: [],
    ping: [],
    auth: [],
  };

  const rulesMaps = {
    recognizers: {},
    blacklist: {},
    headers: {},
    ping: {},
    auth: {},
  };

  const rulesCounts = {
    recognizers: 0,
    blacklist: 0,
    headers: 0,
    ping: 0,
    auth: 0,
  };

  const callbacksMap = {
    domainEquals: _domainEquals,
    domainContains: _domainContains,
    domainStarts: _domainStarts,
    domainEnds: _domainEnds,
    pathEquals: _pathEquals,
    pathContains: _pathContains,
    pathStarts: _pathStarts,
    pathEnds: _pathEnds,
    regExp: _regExp,
    urlEquals: _urlEquals,
  };

  const resourcesTimes = {}; // TODO: can we reset this map when navigation happens?

  function _domainEquals(resource, term) {
    const matched = term === '*' || resource.domain() === term;
    return matched;
  }

  function _domainContains(resource, term) {
    const matched = resource.domain().indexOf(term) > -1;
    return matched;
  }

  function _domainStarts(resource, term) {
    const matched = resource.domain().indexOf(term) === 0;
    return matched;
  }

  function _domainEnds(resource, term) {
    const tIndex = resource.domain().indexOf(term);
    const matched = tIndex > -1 && tIndex === (resource.domain().length - term.length);
    return matched;
  }

  function _pathEquals(resource, term) {
    const matched = term === '*' || resource.path() === term;
    return matched;
  }

  function _pathContains(resource, term) {
    const matched = resource.path().indexOf(term) > -1;
    return matched;
  }

  function _pathStarts(resource, term) {
    const matched = resource.path().indexOf(term) === 0;
    return matched;
  }

  function _pathEnds(resource, term) {
    const tIndex = resource.path().indexOf(term);
    const matched = tIndex > -1 && tIndex === (resource.path().length - term.length);
    return matched;
  }

  function _regExp(resource, term) {
    let matched = false;
    try {
      matched = new RegExp(term).test(resource.url());
    } catch (e) {
      matched = false;
    }
    return matched;
  }

  function _urlEquals(resource, term) {
    const matched = term === '*' || resource.url() === term;
    return matched;
  }

  function _init() {
    // TODO: provide ability to use an object that will set recognizers, blacklist, headers
    return self;
  }

  /**
   * Returns rules in blacklist stack.
   * @return {Array<RecognizerRule>}
   */
  this.blacklist = () => { // eslint-disable-line arrow-body-style
    // TODO: setter
    return rules.blacklist;
  };

  /**
   * Adds a rule in blacklist stack, if recognizer param is not empty a rule in recognizers stack will be added too.
   * If a request triggers the rule, it will be aborted.
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @param {string} [recognizer] recognizer name.
   * @return {boolean} true if the rule is added, false otherwise.
   */
  this.addBlacklistRule = (type, value, recognizer = '') => {
    if (!type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    if (recognizer) {
      self.addRecognizer(recognizer, type, value);
    }
    const mapKey = `${type}||${value}`;
    if (typeof rulesMaps.blacklist[mapKey] !== 'undefined') {
      return false;
    }
    const rule = { type, value };
    rules.blacklist.push(rule);
    rulesMaps.blacklist[mapKey] = rulesCounts.blacklist; // index
    rulesCounts.blacklist++;
    return true;
  };

  /**
   * Removes a rule from blacklist stack, if recognizer param is not empty a recognizer rule will be removed too.
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @param {string} [recognizer] recognizer name.
   * @return {boolean} true if the rule is removed, false otherwise.
   */
  this.removeBlacklistRule = (type, value, recognizer = '') => {
    if (!type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    const mapKey = `${type}||${value}`;
    if (typeof rulesMaps.blacklist[mapKey] === 'undefined') {
      return false;
    }
    if (recognizer) {
      self.removeRecognizer(recognizer, type, value);
    }
    const ruleIndex = rulesMaps.blacklist[mapKey];
    rules.blacklist.splice(ruleIndex, 1);
    delete rulesMaps.blacklist[mapKey];
    rulesCounts.blacklist--;
    return true;
  };

  /**
   * Removes all rules from the blacklist stack.
   */
  this.flushBlacklistRules = () => {
    rules.blacklist = [];
    rulesMaps.blacklist = {};
    rulesCounts.blacklist = 0;
  };

  /**
   * Returns rules in recognizers stack.
   * @return {Array<RecognizerRule>}
   */
  this.recognizers = () => { // eslint-disable-line arrow-body-style
    // TODO: setter
    return rules.recognizers;
  };

  /**
   * Adds a rule in recognizers stack.
   * @param {string} name
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @return {boolean} true if the rule is added, false otherwise.
   */
  this.addRecognizer = (name, type, value) => {
    if (!name || !type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    const mapKey = `${type}||${value}||${name}`;
    if (typeof rulesMaps.recognizers[mapKey] !== 'undefined') {
      return false;
    }
    const rule = { name, type, value };
    rules.recognizers.push(rule);
    rulesMaps.recognizers[mapKey] = rulesCounts.recognizers; // index
    rulesCounts.recognizers++;
    return true;
  };

  /**
   * Removes a rule from recognizers stack.
   * @param {string} name
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @return {boolean} true if the rule is removed, false otherwise.
   */
  this.removeRecognizer = (name, type, value) => {
    if (!name || !type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    const mapKey = `${type}||${value}||${name}`;
    if (typeof rulesMaps.recognizers[mapKey] === 'undefined') {
      return false;
    }
    const ruleIndex = rulesMaps.recognizers[mapKey];
    rules.recognizers.splice(ruleIndex, 1);
    delete rulesMaps.recognizers[mapKey];
    rulesCounts.recognizers--;
    return true;
  };

  /**
   * Removes all rules from the recognizers stack.
   */
  this.flushRecognizers = () => {
    rules.recognizers = [];
    rulesMaps.recognizers = {};
    rulesCounts.recognizers = 0;
  };

  /**
   * Returns rules in headers stack.
   * @return {Array<HeaderRule>}
   */
  this.customHeaders = () => { // eslint-disable-line arrow-body-style
    // TODO: setter
    return rules.headers;
  };

  /**
   * Add a rule in headers stack. If another rules exists for the target header, it will be overwrited.
   * If a request triggers the rule, the target header will be updated consistently.
   * @param {string} domain domain for what the rule is to trigger.
   * @param {string} name name of the custom header.
   * @param {string} value value of the custom header.
   * @return {boolean} true if the rule is added, false otherwise.
   */
  this.addCustomHeader = (domain, name, value) => {
    if (!domain || !name || !value) {
      return false;
    }
    if (!NetworkUtils.testCookieDomain(domain)) {
      return false;
    }
    if (reservedHeaderNames.indexOf(name.toLowerCase()) > -1) {
      return false;
    }
    if (typeof rulesMaps.headers[domain] === 'undefined') {
      rulesMaps.headers[domain] = {};
    }
    const rule = { domain, name, value };
    if (typeof rulesMaps.headers[domain][name] !== 'undefined') {
      const ruleIndex = rulesMaps.headers[domain][name];
      rules.headers[ruleIndex] = rule;
      return true;
    }
    rules.headers.push(rule);
    rulesMaps.headers[domain][name] = rulesCounts.headers; // index
    rulesCounts.headers++;
    return true;
  };

  /**
   * Removes a rule from headers stack.
   * @param {string} domain domain for what the rule is to trigger.
   * @param {string} name name of the custom header.
   * @param {string} [value] value of the custom header.
   * @return {boolean} true if the rule is removed, false otherwise.
   */
  this.removeCustomHeader = (domain, name, value = undefined) => {
    if (!domain || !name) {
      return false;
    }
    if (typeof rulesMaps.headers[domain] === 'undefined' || typeof rulesMaps.headers[domain][name] === 'undefined') {
      return false;
    }
    const checkValue = typeof value !== 'undefined';
    const ruleIndex = rulesMaps.headers[domain][name];
    if (checkValue && rules.headers[ruleIndex].value !== value) {
      return false;
    }
    rules.headers.splice(ruleIndex, 1);
    delete rulesMaps.headers[domain][name];
    rulesCounts.headers--;
    return true;
  };

  /**
   * Removes all rules from headers stack.
   */
  this.flushCustomHeaders = () => {
    rules.headers = [];
    rulesMaps.headers = {};
    rulesCounts.headers = 0;
  };

  /**
   * Returns rules in ping stack
   * @return {Array<PingRule>}
   */
  this.pingRecognizers = () => { // eslint-disable-line arrow-body-style
    // TODO: setter
    return rules.ping;
  };

  /**
   * Adds a rule in the ping stack.
   * If a request triggers a ping rule, it will not be observed by ResourcesMonitor.
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @param {string} [method=GET] method used in order to perform the request.
   * @return {boolean} true if the rule is added, false otherwise.
   */
  this.addPingRecognizer = (type, value, _method = 'GET') => {
    if (!type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    const method = _method.toUpperCase();
    const mapKey = `${method}||${type}||${value}`;
    if (typeof rulesMaps.ping[mapKey] !== 'undefined') {
      return false;
    }
    const rule = { method, type, value };
    rules.ping.push(rule);
    rulesMaps.ping[mapKey] = rulesCounts.ping; // index
    rulesCounts.ping++;
    return true;
  };

  /**
   * Removes a rule from the ping stack.
   * @param {string} type name of the callback to use.
   * @param {string} value comparaison term to pass to the callback.
   * @param {string} [method=GET] method used in order to perform the request.
   * @return {boolean} true if the rule is removed, false otherwise.
   */
  this.removePingRecognizer = (type, value, _method = 'GET') => {
    if (!type || !value || typeof callbacksMap[type] !== 'function') {
      return false;
    }
    const method = _method.toUpperCase();
    const mapKey = `${method}||${type}||${value}`;
    if (typeof rulesMaps.ping[mapKey] === 'undefined') {
      return false;
    }
    const ruleIndex = rulesMaps.ping[mapKey];
    rules.ping.splice(ruleIndex, 1);
    delete rulesMaps.ping[mapKey];
    rulesCounts.ping--;
    return true;
  };

  /**
   * Removes all rules from the ping stack.
   */
  this.flushPingRecognizers = () => {
    rules.ping = [];
    rulesMaps.ping = {};
    rulesCounts.ping = 0;
  };

  /**
   * Returns rules in auth stack
   * @return {Array<AuthRule>}
   */
  this.authenticators = () => { // eslint-disable-line arrow-body-style
    // TODO: setter
    return rules.auth;
  };

  /**
   * Adds a rule in the auth stack. If another rule exists for the target host, it will be overwrited.
   * If a request triggers the rule, the HTTP authentication will be performed consistently.
   * @param {string} host
   * @param {string} username
   * @param {string} password
   * @return {boolean} true if the rule is added, false otherwise.
   */
  this.addAuthenticator = (host, username, password) => {
    if (!host) {
      return false;
    }
    if (!NetworkUtils.testDomain(host)) {
      return false;
    }
    const rule = { host, username, password };
    if (typeof rulesMaps.auth[host] !== 'undefined') {
      const ruleIndex = rulesMaps.auth[host];
      rules.auth[ruleIndex] = rule;
      return true;
    }
    rules.auth.push(rule);
    rulesMaps.auth[host] = rulesCounts.auth; // index
    rulesCounts.auth++;
    return true;
  };

  /**
   * Removes a rule from the auth stack.
   * @param {string} host
   * @return {boolean} true if the rule is removed, false otherwise.
   */
  this.removeAuthenticator = (host) => {
    if (!host) {
      return false;
    }
    if (typeof rulesMaps.auth[host] === 'undefined') {
      return false;
    }
    const ruleIndex = rulesMaps.auth[host];
    rules.auth.splice(ruleIndex, 1);
    delete rulesMaps.auth[host];
    rulesCounts.auth--;
    return true;
  };

  /**
   * Removes all rules from the auth stack.
   */
  this.flushAuthenticators = () => {
    rules.auth = [];
    rulesMaps.auth = {};
    rulesCounts.auth++;
  };

  /**
   * Generates and returns a resource
   * @param {Puppeteer.Request|Puppeteer.Response} [internalResource]
   * @return {Resource}
   */
  this.resource = (internalResource) => {
    if (!internalResource) {
      return new Resource(); // Generates an about:blank resource
    }
    const request = typeof internalResource.request === 'function' ? internalResource.request() : internalResource;
    const interceptionId = request._interceptionId;
    const timing = typeof resourcesTimes[interceptionId] !== 'undefined' ? resourcesTimes[interceptionId] : null;
    const resource = new Resource(internalResource, timing, self);
    return resource;
  };

  /**
   * Saves the start time of a resource, it will be used in order to build SimpleTiming instances
   * @param {Puppeteer.Request|Puppeteer.Response} internalResource
   * @param {number} [time=now] timestamp
   */
  this.setStartTime = (internalResource, time = new Date().getTime()) => {
    const request = typeof internalResource.request === 'function' ? internalResource.request() : internalResource;
    const interceptionId = request._interceptionId;
    if (typeof resourcesTimes[interceptionId] === 'undefined') {
      resourcesTimes[interceptionId] = { startTime: time };
    }
  };

  /**
   * Saves the end time of a resource, it will be used in order to build SimpleTiming instances
   * @param {Puppeteer.Request|Puppeteer.Response} internalResource
   * @param {number} [time=now] timestamp
   */
  this.setEndTime = (internalResource, time = new Date().getTime()) => {
    const request = typeof internalResource.request === 'function' ? internalResource.request() : internalResource;
    const interceptionId = request._interceptionId;
    if (typeof resourcesTimes[interceptionId] !== 'undefined' && typeof resourcesTimes[interceptionId].endTime === 'undefined') {
      resourcesTimes[interceptionId].endTime = time;
    }
  };

  /**
   * Returns if the resource is blacklisted
   * @param {Resource} resource
   * @return {boolean}
   */
  this.isBlacklisted = (resource) => {
    if (!rulesCounts.blacklist) {
      return false;
    }
    let found = false;
    for (let i = 0; i < rulesCounts.blacklist && !found; i++) {
      const rule = rules.blacklist[i];
      if (callbacksMap[rule.type](resource, rule.value)) {
        found = true;
      }
    }
    return found;
  };

  /**
   * Returns the RecognizerRule triggered by the resource
   * @param {Resource} resource
   * @return {?RecognizerRule} null if no rule is triggered
   */
  this.recognizedAs = (resource) => {
    if (!rulesCounts.recognizers) {
      return null;
    }
    let recognizer = null;
    for (let i = 0; i < rulesCounts.recognizers && recognizer === null; i++) {
      const rule = rules.recognizers[i];
      if (callbacksMap[rule.type](resource, rule.value)) {
        recognizer = { name: rule.name, type: rule.type, value: rule.value };
      }
    }
    return recognizer;
  };

  /**
   * Returns if the resource is considered as a ping
   * @param {Resource} resource
   * @return {boolean}
   */
  this.isPing = (resource) => {
    if (!rulesCounts.ping) {
      return false;
    }
    let found = false;
    for (let i = 0; i < rulesCounts.ping && !found; i++) {
      const rule = rules.ping[i];
      if (rule.method === resource.method() && callbacksMap[rule.type](resource, rule.value)) {
        found = true;
      }
    }
    return found;
  };

  /**
   * Returns the http credentials to use to request the resource
   * @param {Resource} resource
   * @return {?HttpCredentials} null if no credentials
   */
  this.credentials = (resource) => {
    const domain = resource.domain();
    return typeof rulesMaps.auth[domain] !== 'undefined' ? rules.auth[rulesMaps.auth[domain]] : null;
  };

  /**
   * Returns the overrides to apply to the request
   * @param {Resource} resource
   * @return {RequestOverrides}
   */
  this.overrides = (resource) => {
    const overrides = {};
    const domain = resource.domain();
    const domainParts = domain.split('.');
    const domainPartsCount = domainParts.length;
    // Setting custom headers
    let found = false;
    for (let i = 0; i < domainPartsCount && !found; i++) {
      const term = (i > 0 ? '.' : '') + domainParts.slice(i).join('.');
      let target = {};
      if (typeof rulesMaps.headers[term] !== 'undefined') {
        found = true;
        overrides.headers = {};
        target = rulesMaps.headers[term];
      }
      Object.keys(target).forEach((key) => {
        const ruleIndex = target[key];
        const rule = rules.headers[ruleIndex];
        overrides.headers[rule.name] = rule.value;
      });
    }
    return overrides;
  };

  /**
   * Processes a Resource by appling needed customizations and then performs the HTTP request
   * @param {Resource} resource
   */
  this.process = (resource) => {
    const request = resource.internal();
    // Setting authenticators
    const credentials = self.credentials(resource);
    if (credentials) {
      request.authenticate(credentials);
    }
    request.continue(self.overrides(resource));
  };

  /**
   * Discard a resource by aborting the HTTP request
   * @param {Resource} resource
   */
  this.discard = (resource) => {
    resource.internal().abort('aborted');
  };

  return _init();
}

/**
 * @typedef {Object} RecognizerRule
 * @property {string} type name of the callback to use.
 * @property {string} value comparaison term to pass to the callback.
 * @property {string} [name]
 */

/**
 * @typedef {Object} HeaderRule
 * @property {string} domain domain for what the rule is to trigger.
 * @property {string} name name of the custom header.
 * @property {string} value value of the custom header.
 */

/**
 * @typedef {Object} PingRule
 * @property {string} method method used in order to perform the request.
 * @property {string} type name of the callback to use.
 * @property {string} value comparaison term to pass to the callback.
 */

/**
 * @typedef {Object} AuthRule
 * @property {string} host
 * @property {string} username
 * @property {string} password
 */

/**
 * @typedef {Object} SimpleTiming
 * @property {number} startTime Timestamp for the time a resource is requested.
 * @property {number} endTime Timestamp for the time a resource has finished to be processed.
 */

/**
 * @typedef {Object} HttpCredentials
 * @property {string} username
 * @property {string} password
 */

/**
 * @typedef {Object} RequestOverrides
 * @property {?string} url rewritten url of the request.
 * @property {?Object} headers an object with addition HTTP headers associated with the request.
 */

module.exports = ResourcesManager;
