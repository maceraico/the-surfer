const puppeteer = require('puppeteer-core');
const Workspace = require('./Workspace.js');
const Logger = require('./Logger.js');
const NetworkUtils = require('./NetworkUtils.js');

const validators = {
  proxy: value => typeof value === 'string',
  screen: screen => typeof screen === 'object' && typeof screen.width === 'number' && typeof screen.height === 'number',
  userAgent: value => typeof value === 'string',
  viewport: value => typeof value === 'object' && typeof value.width === 'number' && typeof value.height === 'number',
  networkCache: value => typeof value === 'boolean',
  networkConditions: (options) => {
    if (typeof options !== 'object') {
      return false;
    }
    const validation = {
      offline: typeof options.offline === 'undefined' || typeof options.offline === 'boolean',
      latency: typeof options.offline === 'undefined' || typeof options.latency === 'number',
      downloadThroughput: typeof options.offline === 'undefined' || typeof options.downloadThroughput === 'number',
      uploadThroughput: typeof options.offline === 'undefined' || typeof options.uploadThroughput === 'number',
    };
    return validation.offline && validation.latency && validation.downloadThroughput && validation.uploadThroughput;
  },
  geolocation: value => typeof value.latitude === 'number' && typeof value.longitude === 'number',
  onInitScripts: value => Array.isArray(value),
  onLoadScripts: value => Array.isArray(value),
  hosts: (value) => {
    if (!Array.isArray(value)) {
      return false;
    }
    let isValid = true;
    value.forEach((host) => {
      if (typeof host !== 'object' || !NetworkUtils.testDomain(host.domain) || !NetworkUtils.testIp(host.ip)) {
        isValid = false;
      }
    });
    return isValid;
  },
};

const argGenerator = {
  proxy: address => `--proxy-server=${address}`,
  screen: screen => `--window-size=${screen.width},${screen.height}`,
  hosts: (hosts) => {
    const hostRules = [];
    hosts.forEach((host) => {
      hostRules.push(`MAP ${host.domain} ${host.ip}`);
    });
    return `--host-rules=${hostRules.join(',')}`;
  },
};

const parseArg = (arg) => {
  if (typeof arg !== 'string' || arg.length < 3 || arg.indexOf('--') !== 0) {
    return null;
  }
  const parsedArg = { name: '', value: '', string: '' };
  const separatorIndex = arg.indexOf('=');
  parsedArg.name = arg.substring(2);
  if (separatorIndex > -1) {
    parsedArg.name = parsedArg.name.substring(0, parsedArg.name.indexOf('='));
    parsedArg.string = `--${parsedArg.name}`;
    parsedArg.value = arg.substring(separatorIndex + 1).trim();
    if (parsedArg.value) {
      parsedArg.string += `=${parsedArg.value}`;
    }
  } else {
    parsedArg.string = `--${parsedArg.name}`;
  }
  return parsedArg;
};

let print = (...args) => { console.log(...args); }; // eslint-disable-line no-console

let greetingsTitle = 'Starting TheSurfer';

let greetingsSeparator = '-'.repeat(greetingsTitle.length);

let greetingsExtras = [];

const defaults = {
  userAgent: '',
  viewport: { width: 1280, height: 800 },
  screen: { width: 1280, height: 800 },
  networkCache: true,
};

/**
 * Surfer module provides a method to launch a Puppeteer instance and generates the workspace associated.
 * @constructor
 */
class Surfer {
  /**
   * Launches a Puppeteer instance and generates the workspace associated.
   * @param {Object} options
   * @param {boolean} [options.greetings=true] Defines if greetings will be printed on stdout.
   * @param {string} [options.greetingsTitle='Starting TheSurfer'] Defines the greetings title.
   * @param {Array<string>} [options.greetingsExtras] Defines the extra lines to print in greetings.
   * @param {LoggerConf} [options.loggerConf] If defined, it will be passed to {@link Logger}
   * @param {WorkspaceWatcherConf} [options.watcherConf] If defined, it will be passed to {@link WorkspaceWatcher}
   * @param {string} [options.userAgent] Specific user agent to use for all pages. Defaults to Puppeteer.Browser.userAgent().
   * @param {string} [options.proxy] If defined, the specified proxy server will be used. It only affects HTTP and HTTPS requests.
   * @param {ScreenConf} [options.screen] Screen configuration object. Defaluts to an 1280x800 screen.
   * @param {ViewportConf} [options.viewport] Viewport configuration object. Defaluts to an 1280x800 viewport.
   * @param {boolean} [options.networkCache] If defined, enable/disable network cache for each page in workspace. Defaults to true.
   * @param {NetworkConditionsConf} [options.networkConditions] Custom network conditions to use. By default no custom network conditions are set.
   * @param {GeolocationConf} [options.geolocation] Geolocation configuration object. By default the geolocation is not available.
   * @param {Array<string>} [options.onInitScripts] List of files to evaluate when a frame is initialized.
   * @param {Array<string>} [options.onLoadScripts] List of files to evaluate when a frame is loaded.
   * @param {Array<string>} [options.args] Additional arguments to pass to the Puppeteer.Browser instance. The list of Chromium flags can be found {@link https://peter.sh/experiments/chromium-command-line-switches/|here}.
   * @param {boolean} [options.sslValidation=true] If true the ssl validation of request will be performed, false otherwise.
   * @param {boolean} [options.dataDir] Path to a {@link https://chromium.googlesource.com/chromium/src/+/master/docs/user_data_dir.md|Data Directory}.
   * @param {string} [options.chromePath] Path to a Chromium or Chrome executable to run instead of the bundled Chromium.
   * @param {Object} [options.puppeteerOpts] Additional options to pass to the {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#puppeteerlaunchoptions|Pupperter.launch}.
   * @return {Promise<Workspace>} Promise which resolves to workspace associated to Puppeteer instance.
   */
  static launch(userOptions) {
    if (userOptions.greetings === false) {
      print = () => {};
    }
    if (typeof userOptions.greetingsTitle === 'string') {
      ({ greetingsTitle } = userOptions);
      greetingsSeparator = '-'.repeat(greetingsTitle.length);
    }
    if (Array.isArray(userOptions.greetingsExtras)) {
      greetingsExtras = userOptions.greetingsExtras.filter(extra => typeof extra === 'string');
    }

    print('');
    print(greetingsSeparator);
    print(greetingsTitle);
    print(greetingsSeparator);

    if (typeof userOptions.loggerConf === 'object') {
      Logger.configuration(userOptions.loggerConf);
    }

    const args = [];
    const argsNames = [];
    const extraFeatures = ['NetworkService', 'NetworkServiceInProcess'];
    if (Array.isArray(userOptions.args)) {
      userOptions.args.forEach((arg) => {
        const parsedArg = parseArg(arg);
        if (parsedArg !== null) {
          if (parseArg.name === 'enable-features') {
            const tmpFeatures = parseArg.value.trim().split(',');
            tmpFeatures.forEach((value) => {
              const feature = value.trim();
              if (extraFeatures.indexOf(feature) === -1) {
                extraFeatures.push(feature);
              }
            });
          } else {
            argsNames.push(parsedArg.name);
            args.push(parsedArg.string);
          }
        }
      });
    }
    argsNames.push('enable-features');
    args.push(`--enable-features=${extraFeatures.join(',')}`);

    const options = JSON.parse(JSON.stringify(userOptions));
    delete options.args;
    delete options.sslValidation;
    delete options.dataDir;
    delete options.chromePath;
    delete options.greetings;
    delete options.greetingsTitle;
    delete options.greetingsExtras;
    delete options.puppeteerOpts;
    delete options.loggerConf;
    delete options.watcherConf;

    const conf = JSON.parse(JSON.stringify(defaults));
    Object.keys(options).forEach((key) => {
      if (typeof validators[key] === 'function' && validators[key](options[key])) {
        const isArgument = typeof argGenerator[key] === 'function';
        if (isArgument) {
          const arg = argGenerator[key](options[key]);
          if (arg) {
            const parsedArg = parseArg(arg);
            if (argsNames.indexOf(parsedArg.name) === -1) {
              args.push(parsedArg.string);
              argsNames.push(parsedArg.name);
              print(`${key}:`, JSON.stringify(options[key]));
            } else {
              delete options[key];
              print(`${key}:`, 'IGNORED OPTION');
            }
          }
        } else {
          conf[key] = options[key];
          print(`${key}:`, JSON.stringify(options[key]));
        }
      } else {
        print(`${key}:`, 'NOT VALID OPTION');
      }
    });

    const puppeteerOpts = { args };
    if (typeof userOptions.chromePath === 'string') {
      puppeteerOpts.executablePath = userOptions.chromePath;
      print('chromePath:', userOptions.chromePath);
    }
    if (typeof userOptions.sslValidation === 'boolean') {
      puppeteerOpts.ignoreHTTPSErrors = !userOptions.sslValidation;
      print('sslValidation:', userOptions.sslValidation);
    } else {
      puppeteerOpts.ignoreHTTPSErrors = false;
    }
    if (typeof userOptions.dataDir === 'string') {
      puppeteerOpts.userDataDir = userOptions.dataDir;
      print('dataDir:', userOptions.dataDir);
    }
    if (typeof userOptions.puppeteerOpts !== 'undefined') {
      Object.keys(userOptions.puppeteerOpts).forEach((key) => {
        if (typeof puppeteerOpts[key] === 'undefined') {
          const value = userOptions.puppeteerOpts[key];
          puppeteerOpts[key] = value;
        }
      });
    }

    greetingsExtras.forEach((extra) => {
      print(extra);
    });

    print('');
    print('');

    return new Promise((resolve, reject) => {
      puppeteer.launch(puppeteerOpts).then(async (browser) => {
        const mainPage = await browser.newPage();
        if (conf.userAgent) {
          await mainPage.setUserAgent(conf.userAgent);
        } else {
          conf.userAgent = await browser.userAgent();
        }
        await Promise.all([
          mainPage.setViewport(conf.viewport),
          mainPage.setCacheEnabled(conf.networkCache),
          mainPage.setRequestInterception(true),
        ]);
        const workspace = new Workspace(browser, mainPage, conf);
        workspace.once('ready', () => {
          if (typeof userOptions.watcherConf === 'object') {
            workspace.watcher().configuration(userOptions.watcherConf);
          }
          resolve(workspace);
        });
      }).catch(error => reject(error));
    });
  }

  /**
   * Returns the singleton instance of Logger
   * @return {Logger}
   */
  static Logger() {
    return Logger;
  }

  /**
   * Returns the LoggerClient instance associated to the provided name.
   * @param {string} name the name of the logger.
   * @param {string} [verbosity] if passed a valid verbosity level, it will be applied.
   * @return {LoggerClient}
   */
  static logger(name, verbosity) {
    return Logger.client(name, verbosity);
  }

  /**
   * Passes the configuration to {@link Logger}
   * @param {LoggerConf} conf
   */
  static loggerConf(conf) {
    Logger.configuration(conf);
  }

  /**
   * Returns NetworkUtils module
   * @return {NetworkUtils}
   */
  static NetworkUtils() {
    return NetworkUtils;
  }
}

/**
 * @typedef {Object} ScreenConf
 * @property {number} width device screen width in pixel. Defaults to 1280.
 * @property {number} height device screen height in pixel. Defaults to 800.
 */

/**
 * @typedef {Object} ViewportConf
 * @property {number} width page width in pixels. Defaults to 1280.
 * @property {number} height page height in pixels. Defaults to 800.
 * @property {boolean} isMobile Whether the meta viewport tag is taken into account. Defaults to false.
 * @property {boolean} hasTouch Specifies if viewport supports touch events. Defaults to false.
 * @property {boolean} isLandscape Specifies if viewport is in landscape mode. Defaults to false.
 */

/**
 * @typedef {Object} NetworkConditionsConf
 * @property {boolean} offline True to emulate internet disconnection, other properties will be set to default value. Defaults to false.
 * @property {number} latency Minimum latency from request sent to response headers received (ms). Defaults to 0.
 * @property {number} downloadThroughput Maximal aggregated download throughput (bytes/sec). -1 disables download throttling. Defaults to -1.
 * @property {number} uploadThroughput Maximal aggregated upload throughput (bytes/sec). -1 disables upload throttling. Defaults to -1.
 */

/**
 * @typedef {Object} GeolocationConf
 * @property {number} latitude The latitude
 * @property {number} longitude The longitude
 * @property {number} accuracy The accuracy in meters
 */

module.exports = Surfer;
