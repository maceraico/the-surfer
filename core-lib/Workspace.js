const { inherits } = require('util');
const process = require('process');
const fs = require('fs');
const path = require('path');
const logger = require('./Logger.js').client('Workspace');
const domLogger = require('./Logger.js').client('DOMLog');
const EventEmitter = require('events');
const ResourcesManager = require('./ResourcesManager.js');
const WorkspaceWatcher = require('./WorkspaceWatcher.js');
const Registry = require('./Registry.js');

/**
 * Workspace is the core class of the framework and represents the context where you are working on.
 * It provides an hierarchy that allows you to move through pages and frames in order
 * to switch the current context. This context is monitored and if something happens
 * it will be changed (e.g.: the target frame is removed from the page).
 * @constructor
 * @param {Puppeteer.Browser} browser Instance of Puppeter's browser. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-browser|Browser}.
 * @param {Puppeteer.Page} mainPage Instance of Puppeter's page, the navigation will occur on it. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
 * @param {Object} conf Workspace configuration object.
 * @param {string} conf.userAgent User agent to use for all pages.
 * @param {Array<string>} [conf.onInitScripts] List of files to evaluate when a frame is initialized.
 * @param {Array<string>} [conf.onLoadScripts] List of files to evaluate when a frame is loaded.
 * @param {GeolocationConf} [conf.geolocation] Geolocation configuration object.
 * @emits Workspace#ready
 * @emits Workspace#waiting-new-page
 * @emits Workspace#page-initialized
 * @emits Workspace#page-created
 * @emits Workspace#page-navigated
 * @emits Workspace#page-changed
 * @emits Workspace#page-destroyed
 * @emits Workspace#dialog
 * @emits Workspace#resource-timing-processed
 * @extends {EventEmitter}
 */
function Workspace(_browser, _mainPage, _conf) {
  const self = this;

  const defaults = {
    onInitScripts: {
      '../dom-lib/Logger.js': fs.readFileSync(path.resolve(__dirname, '../dom-lib/Logger.js'), 'utf8'),
      '../dom-lib/Monitor.js': fs.readFileSync(path.resolve(__dirname, '../dom-lib/Monitor.js'), 'utf8'),
      '../dom-lib/Snapshot.js': fs.readFileSync(path.resolve(__dirname, '../dom-lib/Snapshot.js'), 'utf8'),
      '../dom-lib/Geolocation.js': fs.readFileSync(path.resolve(__dirname, '../dom-lib/Geolocation.js'), 'utf8'),
    },
    onLoadScripts: {
      '../dom-lib/sizzle.min.js': fs.readFileSync(path.resolve(__dirname, '../dom-lib/sizzle.min.js'), 'utf8'),
    },
  };

  const conf = _conf;

  const browser = _browser;

  const mainPage = _mainPage;

  let currPage = mainPage;

  let currFrame = mainPage.mainFrame();

  const pagesIdMap = {};

  const pagesToId = new WeakMap();

  const framesIdMap = {};

  const framesToPageMap = new WeakMap();

  const pagesToDialog = new WeakMap();

  const mainRequests = new WeakMap();

  const mainTimings = new WeakMap();

  const registry = new Registry();

  const snapshotConf = {
    replacedNodeName: 'tse-node-replaced',
    replacedAttrName: 'tse-nodename',
    dataUrlAttrName: 'tse-data-url',
    internalScript: true,
    removeBase: false,
    scriptsToKeepSelector: null,
  };

  const networkConditions = {
    offline: false,
    latency: 0,
    downloadThroughput: -1,
    uploadThroughput: -1,
  };

  const geolocation = {
    enabled: false,
    accuracy: 20,
    latitude: null,
    longitude: null,
  };

  const scriptsToKeepRegExp = new RegExp('type="text/tse-script-to-keep"', 'g');

  let isReady = false;

  let resourcesManager = null;

  let watcher = null;

  let internalPageCreation = false;

  let isNavigationLocked = false;

  let navigationHTML = null;

  let networkCache = true;

  let onInitScripts = {
    entries: JSON.parse(JSON.stringify(defaults.onInitScripts)),
    string: Object.values(defaults.onInitScripts).join('\n\n'),
  };

  let onLoadScripts = {
    entries: JSON.parse(JSON.stringify(defaults.onLoadScripts)),
    string: Object.values(defaults.onLoadScripts).join('\n\n'),
  };

  function _init() {
    const pageId = _generatePageId();
    pagesIdMap[pageId] = mainPage;
    pagesToId.set(mainPage, pageId);
    if (Array.isArray(conf.onInitScripts)) {
      conf.onInitScripts.forEach((script) => {
        self.addOnInitScript(script);
      });
    }
    if (Array.isArray(conf.onLoadScripts)) {
      conf.onLoadScripts.forEach((script) => {
        self.addOnLoadScript(script);
      });
    }
    ({ networkCache } = conf);
    if (conf.networkConditions) {
      self.networkConditions(conf.networkConditions);
    }
    if (conf.geolocation) {
      if (typeof conf.geolocation.latitude === 'number' && typeof conf.geolocation.longitude === 'number') {
        geolocation.enabled = true;
        geolocation.latitude = conf.geolocation.latitude;
        geolocation.longitude = conf.geolocation.longitude;
        if (typeof conf.geolocation.accuracy === 'number') {
          geolocation.accuracy = conf.geolocation.accuracy;
        }
      }
    }
    _frameCreatedListener.apply(mainPage, [currFrame]);
    browser.on('targetcreated', _pageCreatedListener);
    browser.on('targetdestroyed', _pageDestroyedListener);
    _addPageListeners(mainPage);
    resourcesManager = new ResourcesManager();
    watcher = new WorkspaceWatcher(self, mainPage);
    return self;
  }

  function _generatePageId() {
    const id = Math.floor(Math.random() * 0x10000).toString(16) + Math.floor(Math.random() * 0x10000).toString(16);
    return typeof pagesIdMap[id] === 'undefined' ? id : _generatePageId();
  }

  function _getFrameOrigin(frame) {
    if (!frame.parentFrame()) {
      return '*';
    }
    let url = frame.parentFrame().url();
    // Removing hash (if needed)
    const hIndex = url.indexOf('#');
    if (hIndex > -1) {
      url = url.substring(0, hIndex);
    }
    // Removing query (if needed)
    const qIndex = url.indexOf('?');
    if (qIndex > -1) {
      url = url.substring(0, qIndex);
    }
    // Calculating protocol and domain
    const pIndex = url.indexOf('://');
    if (pIndex === -1) {
      return '*';
    }
    const protocol = url.substring(0, pIndex);
    let domain = url.substring(pIndex + 3);
    // Removing path (if needed)
    const sIndex = domain.indexOf('/');
    if (sIndex > -1) {
      domain = domain.substring(0, sIndex);
    }
    return `${protocol}://${domain}`;
  }

  function _addPageListeners(page) {
    page._client.addListener('Page.windowOpen', _windowOpenListener);
    page.addListener('frameattached', _frameCreatedListener);
    page.addListener('framedetached', _frameDestroyedListener);
    page.addListener('framenavigated', _frameNavigatedListener);
    page.addListener('dialog', _dialogListener);
    page.addListener('request', _collectRequestListener);
    page.addListener('load', _pageLoadedListener);
  }

  function _removePageListeners(page) {
    page._client.removeListener('Page.windowOpen', _windowOpenListener);
    page.removeListener('frameattached', _frameCreatedListener);
    page.removeListener('framedetached', _frameDestroyedListener);
    page.removeListener('framenavigated', _frameNavigatedListener);
    page.removeListener('dialog', _dialogListener);
    page.removeListener('request', _collectRequestListener);
    page.removeListener('load', _pageLoadedListener);
  }

  function _windowOpenListener(event) {
    if (internalPageCreation) {
      return;
    }
    self.emit('waiting-new-page', event.url);
    logger.debug('windowOpenListener:', event.url);
  }

  async function _pageCreatedListener(target) {
    if (internalPageCreation) {
      return;
    }
    const page = await target.page();
    if (page) {
      self.emit('page-initialized', page);
      await page.setContent('<html><head></head><body></body></html>');
      const pageId = _generatePageId();
      pagesIdMap[pageId] = page;
      pagesToId.set(page, pageId);
      await Promise.all([
        page.setCacheEnabled(networkCache),
        page.setRequestInterception(true),
      ]);
      page.addListener('request', _requestAborter);
      await Promise.all([
        page.setUserAgent(conf.userAgent),
        page.setViewport(mainPage.viewport()),
        page._client.send('Network.emulateNetworkConditions', networkConditions),
      ]);
      self.emit('page-created', page, pageId);
      currPage = page;
      currFrame = currPage.mainFrame();
      await _frameCreatedListener.apply(currPage, [currFrame]).catch((error) => {
        logger.error('frameCreatedListener error:', error.message);
      });
      page.removeListener('request', _requestAborter);
      _addPageListeners(currPage);
      self.emit('page-changed', page, pageId);
      logger.debug('pageCreatedListener:', pageId);
      page.evaluate(() => {
        window.location.reload(true);
      }).catch((error) => {
        logger.error('window.location.reload error:', error.message);
      });
    }
  }

  async function _pageDestroyedListener(target) {
    let page = null;
    page = await target.page().catch(() => {
      page = null;
    });
    if (page && pagesToId.has(page)) {
      _removePageListeners(page);
      const pageId = pagesToId.get(page);
      delete pagesIdMap[pageId];
      pagesToId.delete(page);
      pagesToDialog.delete(page);
      self.emit('page-destroyed', page, pageId);
      if (page === currPage) {
        currPage = mainPage;
        currFrame = currPage.mainFrame();
        self.emit('page-changed', currPage, pagesToId.get(currPage));
      }
      logger.debug('pageDestroyedListener:', pageId);
    }
  }

  async function _pageLoadedListener() {
    const page = this;
    const frame = page.mainFrame();
    let onLoadInjectionsPerformed = false;
    onLoadInjectionsPerformed = await frame.evaluate(() => typeof window._TSE_onLoadScripts === 'function').catch(() => {
      onLoadInjectionsPerformed = null;
    });
    if (onLoadInjectionsPerformed === false) {
      // This scenario could be verified by loading an invalid url e.g.: http://asdasd
      logger.warning('evaluating onLoadScripts', 'verbose');
      await frame.evaluate(onLoadScripts.string).catch((error) => {
        logger.error('onLoadScripts eval error:', error.message);
      });
    }
  }

  const framesSpecs = {};

  function _frameSpecs(frame) {
    if (typeof framesSpecs[frame._id] !== 'undefined') {
      return framesSpecs[frame._id];
    }
    const specs = {
      id: frame._id,
      origin: '*',
      handle: null,
      announced: false,
    };
    framesSpecs[frame._id] = specs;
    return framesSpecs[frame._id];
  }

  async function _frameHandle(frame) {
    const specs = _frameSpecs(frame);
    if (specs.handle) {
      return specs.handle;
    }
    const parent = frame.parentFrame();
    const errorCallback = (error) => {
      if (typeof framesIdMap[frame._id] !== 'undefined') { // Meanwhile, frame could be destroyed
        logger.error('_frameHandle eval error:', frame._id, error);
      }
    };
    let handles = await parent.$$('iframe').catch(errorCallback);
    if (!handles) {
      handles = [];
    }
    const contentFramesQueue = [];
    handles.forEach((handle) => {
      const contentFramePromise = handle.contentFrame();
      contentFramesQueue.push(contentFramePromise);
      contentFramePromise.then((contentFrame) => {
        if (!contentFrame) {
          return;
        }
        const currSpecs = _frameSpecs(contentFrame);
        currSpecs.handle = handle;
      }).catch((error) => {
        logger.warning('contentFramePromise error:', error.message);
      });
    });
    if (contentFramesQueue.length) {
      await Promise.all(contentFramesQueue).catch((error) => {
        logger.warning('contentFramesQueue error:', error.message);
      });
    }
    return _frameSpecs(frame).handle;
  }

  async function _frameCreatedListener(frame) {
    logger.debug('frameCreatedListener:', frame._id, 'verbose');
    const page = this;
    const parent = frame.parentFrame();
    const specs = _frameSpecs(frame);
    specs.origin = _getFrameOrigin(frame);
    framesIdMap[frame._id] = frame;
    framesToPageMap.set(frame, page);
    if (parent) {
      const errorCallback = (error) => {
        if (typeof framesIdMap[frame._id] !== 'undefined') { // Meanwhile, frame could be destroyed
          logger.error('frameCreatedListener eval error:', frame._id, error, 'dev');
        }
      };
      _frameHandle(frame).then((handle) => {
        if (!handle) {
          return;
        }
        specs.handle = handle;
        parent.evaluate((elm, id) => { // eslint-disable-line no-shadow
          if (!elm || !(elm instanceof HTMLIFrameElement)) {
            return false;
          }
          elm._TSE_frameId = id;
          return true;
        }, specs.handle, specs.id).then((evalResult) => {
          specs.announced = !!evalResult;
        }).catch(errorCallback);
      });
      frame.evaluate((id, origin) => {
        if (typeof window._TSE_ === 'undefined') {
          window._TSE_ = {};
        }
        window._TSE_.frameSpecs = { id, origin };
        if (typeof window._TSE_.Monitor !== 'undefined') {
          window._TSE_.Monitor.triggerReady();
        }
      }, frame._id, specs.origin).catch(errorCallback);
      parent.evaluate((elm, id) => { // eslint-disable-line no-shadow
        if (!elm || !(elm instanceof HTMLIFrameElement)) {
          return false;
        }
        elm._TSE_frameId = id;
        return true;
      }, specs.handle, specs.id).then((evalResult) => {
        specs.announced = !!evalResult;
      }).catch(errorCallback);
      return;
    }
    await Promise.all([
      page.evaluateOnNewDocument(onInitScripts.string),
      page.exposeFunction('_TSE_onLoadScripts', _DOMOnLoadScripts),
      page.exposeFunction('_TSE_resourceTimingProcessed', _DOMResourceTimingProcessed),
      page.exposeFunction('_TSE_internalPageCreation', _DOMInternalPageCreation),
      page.exposeFunction('_TSE_snapshotConf', _DOMSnapshotConf),
      page.exposeFunction('_TSE_logDebug', _DOMLogDebug),
      page.exposeFunction('_TSE_logInfo', _DOMLogInfo),
      page.exposeFunction('_TSE_logSuccess', _DOMLogSuccess),
      page.exposeFunction('_TSE_logWarning', _DOMLogWarning),
      page.exposeFunction('_TSE_logError', _DOMLogError),
      page.exposeFunction('_TSE_emitFileUpload', _DOMEmitFileUpload),
      page.exposeFunction('_TSE_getGeolocation', _DOMGetGeolocation),
    ]);
    if (!isReady) {
      isReady = true;
      self.emit('ready');
      logger.success('workspace is ready');
    }
  }

  function _frameDestroyedListener(frame) {
    logger.debug('frameDestroyedListener:', frame._id, frame.url(), 'verbose');
    delete framesSpecs[frame._id];
    delete framesIdMap[frame._id];
    framesToPageMap.delete(frame);
    mainRequests.delete(frame);
    if (frame === currFrame) {
      currFrame = mainPage.mainFrame();
    }
  }

  async function _frameNavigatedListener(frame) {
    if (typeof framesIdMap[frame._id] === 'undefined') {
      return;
    }
    logger.debug('frameNavigatedListener:', frame._id, frame.url(), 'verbose');
    const parent = frame.parentFrame();
    const specs = _frameSpecs(frame);
    const errorCallback = (error) => {
      if (typeof framesIdMap[frame._id] !== 'undefined') { // Meanwhile, frame could be destroyed
        logger.error('frameNavigatedListener eval error:', frame._id, error, 'dev');
      }
    };
    specs.origin = _getFrameOrigin(frame);
    if (frame.url().indexOf('chrome-error://') === 0) {
      specs.announced = false;
    }
    if (parent && specs.announced === false) {
      _frameHandle(frame).then((handle) => {
        if (!handle) {
          return;
        }
        specs.handle = handle;
        parent.evaluate((elm, id) => { // eslint-disable-line no-shadow
          if (!elm || !(elm instanceof HTMLIFrameElement)) {
            return false;
          }
          elm._TSE_frameId = id;
          return true;
        }, specs.handle, specs.id).then((evalResult) => {
          specs.announced = !!evalResult;
        }).catch(errorCallback);
      });
    }
    const isHTML = await frame.evaluate((id, origin, onInitScripts) => { // eslint-disable-line no-shadow
      if (!(document instanceof HTMLDocument)) {
        window.location.href = 'about:blank';
        return false;
      }
      if (typeof window._TSE_ === 'undefined') {
        window._TSE_ = {};
      }
      if (typeof window._TSE_.Monitor === 'undefined') {
        window.eval(onInitScripts); // eslint-disable-line no-eval
      }
      window._TSE_.frameSpecs = { id, origin };
      window._TSE_.Monitor.triggerReady();
      return true;
    }, frame._id, specs.origin, onInitScripts.string).catch(errorCallback);
    if (isHTML !== true) {
      if (isHTML === false) {
        logger.warning(`${frame._id} is NOT an HTMLDocument, redirecting to about:blank`, 'verbose');
        mainRequests.get(frame).response()._forcedAsBinary = true;
      }
      return;
    }
    if (frame === currPage.mainFrame()) {
      self.emit('page-navigated', currPage, pagesToId.get(currPage));
    }
  }

  function _dialogListener(dialog) {
    const page = this;
    pagesToDialog.set(page, dialog);
    if (page === currPage) {
      self.emit('dialog', dialog);
    }
  }

  async function _requestAborter(request) {
    if (request._interceptionHandled === true) {
      return;
    }
    request.abort();
  }

  function _collectRequestListener(request) {
    if (request.isNavigationRequest()) {
      if (!isNavigationLocked) {
        const frame = request.frame();
        if (frame !== null) {
          resourcesManager.setStartTime(request);
          mainRequests.set(frame, request);
          mainTimings.delete(frame);
        }
      } else if (navigationHTML !== null) {
        request.respond({
          status: 200,
          contentType: 'text/html;',
          body: navigationHTML,
        });
      } else {
        request.abort('aborted');
      }
    }
  }

  async function _setCookies(pages, ...args) {
    const page = pages.pop();
    const toReturn = await page.setCookie(...args);
    return pages.length ? _setCookies(pages, ...args) : toReturn;
  }

  async function _setViewport(pages, viewport) {
    const page = pages.pop();
    const orientationChanged = typeof viewport.isLandscape !== 'undefined' && page.viewport().isLandscape !== viewport.isLandscape;
    const toReturn = await page.setViewport(viewport);
    if (orientationChanged) {
      await page.evaluate((viewport) => { // eslint-disable-line no-shadow
        const width = window.innerWidth;
        const height = window.innerHeight;
        const checkInterval = 100;
        let checkTimer = null;
        const checkCallback = () => {
          checkTimer = clearTimeout(checkTimer);
          if (width === viewport.width && height === viewport.height) {
            window.orientation = viewport.isLandscape ? 90 : 0;
            const event = document.createEvent('HTMLEvents');
            event.initEvent('orientationchange', true, true);
            window.dispatchEvent(event);
          } else {
            checkTimer = setTimeout(checkCallback, checkInterval);
          }
        };
        checkTimer = setTimeout(checkCallback, checkInterval);
      }, page.viewport()).catch((error) => {
        logger.error('setViewport eval error:', error.message);
      });
    }
    return pages.length ? _setViewport(pages, viewport) : toReturn;
  }

  function _snapshot(frameId, type, ...args) {
    return new Promise(async (resolve, reject) => {
      if (typeof framesIdMap[frameId] === 'undefined') {
        reject(new Error('frameId NOT valid'));
        return;
      }
      internalPageCreation = true;
      const snapshotPage = await browser.newPage();
      internalPageCreation = false;
      const frame = framesIdMap[frameId];
      const viewport = await frame.evaluate(() => {
        const windowSize = { width: window.innerWidth, height: window.innerHeight };
        return windowSize;
      }).catch((error) => {
        logger.error('snapshot eval error:', error.message);
      });
      if (typeof viewport !== 'object' || typeof viewport.width !== 'number' || typeof viewport.height !== 'number') {
        reject(new Error('viewport not valid'));
        return;
      }
      const isHidden = viewport.width === 0 || viewport.height === 0;
      if (isHidden) {
        viewport.width = 1;
        viewport.height = 1;
      }
      const mainResource = resourcesManager.resource(mainRequests.get(frame));
      const contentType = mainResource.contentType();
      let html = `<html><head></head><body>${contentType}</body></html>`;
      if (!mainResource.isBinary()) {
        html = await frame.evaluate(() => {
          if (typeof window._TSE_ === 'undefined' || typeof window._TSE_.Snapshot === 'undefined') {
            return '<html><head></head><body></body></html>';
          }
          return window._TSE_.Snapshot.html();
        }).catch((err) => {
          html = null;
          reject(err);
        });
        if (html === null) {
          return;
        }
      }
      try {
        await Promise.all([
          snapshotPage.setUserAgent(conf.userAgent),
          snapshotPage.setViewport(viewport),
          snapshotPage.setCacheEnabled(false),
          snapshotPage.setRequestInterception(true),
        ]);
      } catch (err) {
        reject(err);
      }
      const url = frame.url();
      const requestsStack = [];
      if (url.indexOf('http') === 0) {
        snapshotPage.once('request', async (request) => {
          request.respond({
            status: 200,
            contentType: 'text/html;',
            body: '<html><head></head><body></body></html>',
          });
          snapshotPage.on('request', async (request) => { // eslint-disable-line no-shadow
            requestsStack.push(request._interceptionId);
            request.continue();
          });
          snapshotPage.on('requestfailed', async (request) => { // eslint-disable-line no-shadow
            const stackIndex = requestsStack.indexOf(request._interceptionId);
            if (stackIndex > -1) {
              requestsStack.splice(stackIndex, 1);
            }
          });
          snapshotPage.on('response', async (response) => {
            const stackIndex = requestsStack.indexOf(response.request()._interceptionId);
            if (stackIndex > -1) {
              requestsStack.splice(stackIndex, 1);
            }
          });
        });
        await snapshotPage.goto(frame.url());
      }
      const saveSnapshot = async () => {
        if (type === 'html') {
          let snapPageContent = await snapshotPage.content();
          if (snapshotConf.scriptsToKeepSelector) {
            snapPageContent = snapPageContent.replace(scriptsToKeepRegExp, 'type="text/javascript"');
          }
          fs.writeFile(args[0], snapPageContent, async (err) => {
            if (err) {
              reject(err);
            }
            await snapshotPage.close();
            resolve(true);
          });
          return;
        }
        const buffer = await snapshotPage.screenshot(...args).catch((err) => { reject(err); });
        await snapshotPage.close();
        resolve(buffer);
      };
      if (!isHidden) {
        snapshotPage.once('load', saveSnapshot);
      }
      await snapshotPage.setContent(html).catch((err) => { reject(err); });
      if (snapshotConf.removeBase === true && type === 'html') {
        await snapshotPage.evaluate(() => {
          const baseElms = document.querySelectorAll('head base');
          baseElms.forEach((elm) => {
            elm.parentElement.removeChild(elm);
          });
        }).catch(() => {
          // continue
        });
      }
      if (isHidden) {
        saveSnapshot();
        return;
      }
      if (!requestsStack.length) {
        snapshotPage.emit('load');
      }
    });
  }

  async function _setGeolocation(frames) {
    const frame = frames.pop();
    await frame.evaluate(() => {
      if (typeof navigator.geolocation._TSE_positionUpdated === 'function') {
        navigator.geolocation._TSE_positionUpdated();
      }
    }).catch((error) => {
      logger.error('setGeolocation eval error:', error.message);
    });
    if (frames.length) {
      await _setGeolocation(frames);
    }
  }

  async function _checkInjections(frames) {
    const frame = frames.pop();
    const injectionsStatus = await frame.evaluate(() => {
      const status = { onInit: false, onLoad: false };
      if (typeof window._TSE_ === 'undefined') {
        return status;
      }
      if (typeof window._TSE_.Monitor !== 'undefined') {
        status.onInit = true;
      }
      if (typeof window._TSE_.$ !== 'undefined') {
        status.onLoad = true;
      }
      return status;
    }).catch((error) => {
      logger.error('checkInjections eval error:', error.message);
    });
    if (typeof injectionsStatus !== 'undefined') {
      if (injectionsStatus.onInit === false) {
        await frame.evaluate(onInitScripts.string).catch((error) => {
          logger.error('checkInjections onInitScripts eval error:', error.message);
        });
      }
      if (injectionsStatus.onLoad === false) {
        await frame.evaluate(onLoadScripts.string).catch((error) => {
          logger.error('checkInjections onLoadScripts eval error:', error.message);
        });
      }
    }
    if (frames.length) {
      await _checkInjections(frames);
    }
  }

  function _DOMOnLoadScripts() {
    return onLoadScripts.entries;
  }

  function _DOMLogDebug(...args) {
    return domLogger.debug.apply(self, args);
  }

  function _DOMLogInfo(...args) {
    return domLogger.info.apply(self, args);
  }

  function _DOMLogSuccess(...args) {
    return domLogger.success.apply(self, args);
  }

  function _DOMLogWarning(...args) {
    return domLogger.warning.apply(self, args);
  }

  function _DOMLogError(...args) {
    return domLogger.error.apply(self, args);
  }

  function _DOMResourceTimingProcessed(entry, navigationStart, frameId, fromMainContext) {
    // logger.debug('DOMResourceTimingProcessed:', frameId, navigationStart, entry);
    if (entry.initiatorType === 'iframe') {
      // We don't need to trigger these events since in future a navigation entry will come!
      return;
    }
    let frame = null;
    if (!frameId) {
      if (entry.initiatorType === 'navigation' && entry.decodedBodySize === 0 && entry.encodedBodySize === 0) {
        logger.warning('DOMResourceTimingProcessed:', 'network error', entry.name, 'dev');
        return;
      }
      if (fromMainContext === false) {
        // If frameId is missing, it means that window._TSE_.frameSpecs is not set yet
        Object.values(framesIdMap).some((currentFrame) => {
          if (entry.name.indexOf(currentFrame.url()) === 0) {
            frame = currentFrame;
            return true;
          }
          return false;
        });
      } else {
        frame = mainPage.mainFrame();
      }
    } else {
      frame = framesIdMap[frameId];
    }
    if (!frame) {
      // To trigger this scenario should not be possible if everything work properly
      logger.error('DOMResourceTimingProcessed:', 'unable to map frame', entry.name, 'verbose');
      return;
    }
    const offset = entry.startTime;
    let url = entry.name;
    const hIndex = url.indexOf('#');
    if (hIndex > -1) {
      url = url.substring(0, hIndex);
    }
    const timing = {
      url,
      startTime: navigationStart + entry.startTime,
      fetchStart: entry.fetchStart > 0 ? entry.fetchStart - offset : 0,
      domainLookupStart: entry.domainLookupStart > 0 ? entry.domainLookupStart - offset : 0,
      domainLookupEnd: entry.domainLookupEnd > 0 ? entry.domainLookupEnd - offset : 0,
      connectStart: entry.connectStart > 0 ? entry.connectStart - offset : 0,
      secureConnectionStart: entry.secureConnectionStart > 0 ? entry.secureConnectionStart - offset : 0,
      connectEnd: entry.connectEnd > 0 ? entry.connectEnd - offset : 0,
      requestStart: entry.requestStart > 0 ? entry.requestStart - offset : 0,
      responseStart: entry.responseStart > 0 ? entry.responseStart - offset : 0,
      responseEnd: entry.responseEnd > 0 ? entry.responseEnd - offset : 0,
      size: entry.decodedBodySize || 0,
      sizeEncoded: entry.encodedBodySize || 0,
    };
    if (entry.initiatorType === 'navigation') {
      mainTimings.set(frame, JSON.parse(JSON.stringify(timing)));
    }
    self.emit('resource-timing-processed', timing, frame, framesToPageMap.get(frame));
  }

  function _DOMSnapshotConf() {
    return snapshotConf;
  }

  function _DOMInternalPageCreation(flag) {
    if (typeof flag === 'boolean') {
      internalPageCreation = flag;
    }
    return internalPageCreation;
  }

  async function _DOMEmitFileUpload(frameId, fromMainContext) {
    let frame = null;
    if (!frameId) {
      if (fromMainContext) {
        frame = mainPage.mainFrame();
      }
    } else {
      frame = framesIdMap[frameId];
    }
    if (!frame || frame !== currFrame) {
      return;
    }
    const elementHandle = await frame.evaluateHandle(() => {
      if (typeof window._TSE_ === 'undefined') {
        throw new Error('window._TSE_ is not defined');
      }
      if (typeof window._TSE_.Monitor === 'undefined') {
        throw new Error('window._TSE_.Monitor is not defined');
      }
      const elm = window._TSE_.Monitor.inputFileTouched();
      if (elm === null) {
        throw new Error('element not found');
      }
      return elm;
    }).catch((error) => { // eslint-disable-line no-unused-vars
      logger.error('_DOMEmitFileUpload:', error.message);
    });
    if (elementHandle) {
      self.emit('file-upload', elementHandle);
    }
  }

  function _DOMGetGeolocation() {
    return geolocation;
  }

  /**
   * @return {Puppeteer.Browser} Instance of Puppeter's browser. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-browser|Browser}.
   */
  this.browser = () => browser;

  /**
   * @return {Puppeteer.Page} Puppeteer's instance of current page. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
   */
  this.page = () => currPage;

  /**
   * @return {Array<Puppeteer.Page>} All pages in workspace.
   */
  this.pages = () => Object.values(pagesIdMap);

  /**
   * @return {string} The id of the current page.
   */
  this.pageId = () => pagesToId.get(currPage);

  /**
   * @return {boolean} Indicates if the current page is the main page.
   */
  this.inMainPage = () => currPage === mainPage;

  /**
   * @return {boolean} Indicates if the current page is a child of the main page.
   */
  this.inChildPage = () => currPage !== mainPage;

  /**
   * @return {Array<string>} All ids of the pages opened.
   */
  this.pagesId = () => Object.keys(pagesIdMap);

  /**
   * @return {Array<string>} All ids of the pages opened except for the main page.
   */
  this.childPages = () => {
    const pagesId = self.pagesId();
    return pagesId.splice(pagesId.indexOf(pagesToId.get(mainPage)), 1);
  };

  /**
   * Switches the current context to the main frame of the specified page.
   * @param {string} pageId The id of the page that will become the current page.
   * @return {boolean} true if the current context has been changed, false otherwise.
   * @emits Workspace#page-changed
   */
  this.switchToPage = (pageId) => {
    if (typeof pagesIdMap[pageId] === 'undefined') {
      return false;
    }
    currPage = pagesIdMap[pageId];
    currFrame = currPage.mainFrame();
    self.emit('page-changed', currPage, pageId);
    return true;
  };

  /**
   * Switches the current context to the main frame of the main page.
   * @emits Workspace#page-changed
   */
  this.switchToMainPage = () => {
    currPage = mainPage;
    currFrame = currPage.mainFrame();
    self.emit('page-changed', currPage, pagesToId.get(currPage));
  };

  /**
   * @return {Puppeteer.Frame} Puppeteer's instance of current frame. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-frame|Frame}.
   */
  this.frame = () => currFrame;

  /**
   * @return {string} The id of the current frame.
   */
  this.frameId = () => currFrame._id;

  /**
   * @return {boolean} Indicates if the current frame is the main frame of the current page.
   */
  this.inMainFrame = () => currFrame === currPage.mainFrame();

  /**
   * @return {boolean} Indicates if the current frame is a descendant of the main frame.
   */
  this.inChildFrame = () => currFrame !== currPage.mainFrame();

  /**
   * @return {Array<string>} All ids of parent frames.
   */
  this.parentFramesId = () => {
    const framesTree = [];
    let parentFrame = currFrame.parentFrame();
    while (parentFrame !== null) {
      framesTree.push(parentFrame._id);
      parentFrame = parentFrame.parentFrame();
    }
    return framesTree;
  };

  /**
   * @return {Array<string>} All id of child frames.
   */
  this.childFramesId = () => {
    const childFramesId = [];
    const childFrames = currFrame.childFrames();
    const childFramesCount = childFrames.length;
    for (let i = 0; i < childFramesCount; i++) {
      childFramesId.push(childFrames[i]._id);
    }
    return childFramesId;
  };

  /**
   * Switches the current context to a specific frame of the current page.
   * @param {string} frameId The id of the frame that will become the current frame.
   * @return {boolean} true if the current context has been changed, false otherwise.
   */
  this.switchToFrame = (frameId) => {
    if (typeof framesIdMap[frameId] === 'undefined') {
      return false;
    }
    const frame = framesIdMap[frameId];
    const framePage = framesToPageMap.get(frame);
    if (framePage !== currPage) {
      return false;
    }
    currFrame = frame;
    return true;
  };

  /**
   * If the current frame has a parent, the context will be switched to it.
   * @return {boolean} true if the current frame has been changed, false otherwise.
   */
  this.switchToParentFrame = () => {
    const parentFrame = currFrame.parentFrame();
    if (parentFrame === null) {
      return false;
    }
    currFrame = parentFrame;
    return true;
  };

  /**
   * The current context is changed to main frame of the current page.
   */
  this.switchToMainFrame = () => {
    currFrame = currPage.mainFrame();
  };

  /**
   * @return {boolean} true if the current context is the main frame of the main page, false otherwise.
   */
  this.inMainContext = () => currFrame === mainPage.mainFrame();

  /**
   * The current context is changed to the main frame of the main page.
   * @emits Workspace#page-changed
   */
  this.switchToMainContext = () => {
    if (currPage !== mainPage) {
      currPage = mainPage;
      self.emit('page-changed', currPage, pagesToId.get(currPage));
    }
    currFrame = currPage.mainFrame();
  };

  /**
   * Peforms a javascript evaluation in the current context.
   * @param {function|string} pageFunction Function to be evaluated in the page context.
   * @param {...*} args Arguments to pass to pageFunction.
   * @return {Promise} Promise which resolves to the return value of pageFunction.
   */
  this.evaluate = (...args) => currFrame.evaluate(...args);

  /**
   * Performs a screenshot of the current page.
   * @param {Object} [options] Options object passed to currPage.screenshot {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagescreenshotoptions|Puppeteer's method}.
   * @return {Promise<Buffer|string>} Promise which resolves to buffer or a base64 string (depending on
   * the value of encoding) with captured screenshot.
   */
  this.screenshot = (...args) => currPage.screenshot(...args);

  /**
   * @param {string} [frameId=currFrameId] The frame id for what you want to get the main resource,
   * if not specified the current frame id will be used.
   * @return {?Resource} The resource associated to the specified frame. Returns null if the frame id is not valid.
   */
  this.mainResource = (frameId = currFrame._id) => {
    if (typeof framesIdMap[frameId] === 'undefined') {
      return null;
    }
    const frame = framesIdMap[frameId];
    const request = mainRequests.get(frame);
    if (!request) {
      return resourcesManager.resource(); // Get an about:blank resource
    }
    const timing = mainTimings.get(frame);
    if (timing) {
      if (typeof timing.redirectStart === 'undefined' && typeof timing.redirectEnd === 'undefined') {
        const redirectChain = request.redirectChain();
        const lastRedirectIndex = redirectChain.length - 1;
        let redirectStart = 0;
        let redirectEnd = 0;
        redirectChain.forEach((redirect, index) => {
          const redirectTiming = resourcesManager.resource(redirect).timing();
          if (index === 0) {
            redirectStart = redirectTiming.startTime - timing.startTime;
          }
          if (index === lastRedirectIndex) {
            redirectEnd = redirectTiming.endTime - timing.startTime;
          }
        });
        timing.redirectStart = redirectStart;
        timing.redirectEnd = redirectEnd;
        mainTimings.set(frame, timing);
      }
      const resource = resourcesManager.resource(request);
      resource.timing(timing);
      return resource;
    }
    return resourcesManager.resource(request);
  };

  /**
   * Performs a screenshot of a frame without scroll the page.
   * @param {string} frameId The frame id
   * @param {Object} [options] Options object passed to currPage.screenshot {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagescreenshotoptions|Puppeteer's method}.
   * @return {Promise<Buffer|string>} Promise which resolves to buffer or a base64 string (depending on
   * the value of encoding) with captured screenshot.
   */
  this.snapshot = (frameId, ...args) => _snapshot(frameId, 'screenshot', ...args);

  /**
   * Saves the computed html of a frame
   * @param {string} frameId The frame id.
   * @param {string} path The file path to save the html to.
   * @return {Promise}
   */
  this.htmlSnapshot = (frameId, ...args) => _snapshot(frameId, 'html', ...args);

  /**
   * Defines and returns the current configuration for html snapshots
   * @param {HTMLSnapshotConf} [opts] if passed the current configuration will be replaced.
   * @return {HTMLSnapshotConf}
   */
  this.snapshotConf = (opts) => {
    if (typeof opts === 'object') {
      if (typeof opts.replacedNodeName === 'string') {
        snapshotConf.replacedNodeName = opts.replacedNodeName;
      }
      if (typeof opts.replacedAttrName === 'string') {
        snapshotConf.replacedAttrName = opts.replacedAttrName;
      }
      if (typeof opts.dataUrlAttrName === 'string') {
        snapshotConf.dataUrlAttrName = opts.dataUrlAttrName;
      }
      if (typeof opts.scriptsToKeepSelector === 'string') {
        snapshotConf.scriptsToKeepSelector = opts.scriptsToKeepSelector;
      }
      snapshotConf.removeBase = typeof opts.removeBase === 'boolean' ? opts.removeBase : false;
      snapshotConf.internalScript = typeof opts.internalScript === 'boolean' ? opts.internalScript : true;
    }
    return snapshotConf;
  };

  /**
   * Print as pdf the current page
   * @param {Object} [options] Options object passed to currPage.pdf {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions|Puppeteer's method}.
   * @return {Promise<Buffer>} Promise which resolves with PDF buffer.
   */
  this.pdf = (...args) => currPage.pdf(...args);

  /**
   * Perform a navigation request on current page
   * @param {string} url URL to navigate page to. The url should include protocol.
   * @param {Object} [options] Options object passed to currPage.goto {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagegotourl-options|Puppeteer's method}.
   * @return {Promise<?Puppeteer.Response>} Promise which resolves to the main resource response. In case of multiple redirects,
   * the navigation will resolve with the response of the last redirect.
   */
  this.goto = (...args) => currPage.goto(...args);

  /**
   * @return {Puppeteer.Mouse} The {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-mouse|Mouse} used by workspace.
   */
  this.mouse = () => currPage.mouse;

  /**
   * @return {Puppeteer.Touchscreen} The {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-touchscreen|Touchscreen} used by workspace.
   */
  this.touchscreen = () => currPage.touchscreen;

  /**
   * @return {Puppeteer.Keyboard} The {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-keyboard|Keyboard} used by workspace.
   */
  this.keyboard = () => currPage.keyboard;

  /**
   * @return {Puppeteer.Viewport} The {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pageviewport|Viewport} used by workspace.
   */
  this.viewport = () => mainPage.viewport();

  /**
   * Set passed cookies in all pages of the workspace
   * @param {...Object} cookies Object that describes the cookie to set
   * @param {string} cookies.name required
   * @param {string} cookies.value required
   * @param {string} cookies.url
   * @param {string} cookies.domain
   * @param {string} cookies.path
   * @param {number} cookies.expires unix time in seconds
   * @param {boolean} cookies.httpOnly
   * @param {boolean} cookies.secure
   * @return {Promise}
   */
  this.setCookies = (...args) => {
    const pages = Object.values(pagesIdMap);
    return _setCookies(pages, ...args);
  };

  /**
   * Set viewport for all pages of the workspace
   * @param {Object} viewport Object that describes the viewport
   * @param {number} viewport.width Page width in pixels.
   * @param {number} viewport.height Page height in pixels.
   * @param {number} viewport.deviceScaleFactor Specify device scale factor (can be thought of as dpr). Defaults to 1.
   * @param {boolean} viewport.isMobile Whether the meta viewport tag is taken into account. Defaults to false.
   * @param {boolean} viewport.hasTouch Specifies if viewport supports touch events. Defaults to false.
   * @param {boolean} viewport.isLandscape Specifies if viewport is in landscape mode. Defaults to false.
   * @return {Promise}
   */
  this.setViewport = (...args) => {
    const pages = Object.values(pagesIdMap);
    return _setViewport(pages, ...args);
  };

  /**
   * @return {Promise} Closes Chromium and all of its pages (if any were opened).
   * The Browser object itself is consideredto be disposed and cannot be used anymore.
   */
  this.close = () => browser.close();

  /**
   * @return {HistoryStatus}
   */
  this.historyStatus = async () => {
    let history = {};
    history = await currPage._client.send('Page.getNavigationHistory', {}).catch(() => {
      history = {
        entries: [],
        currentIndex: -1,
      };
    });
    const index = history.currentIndex;
    const entriesCount = history.entries.length;
    const firstEntryIndex = currPage === mainPage ? 1 : 0;
    return {
      canGoBack: entriesCount > 0 && index > firstEntryIndex,
      canGoForward: entriesCount > 0 && index < (entriesCount - 1),
    };
  };

  /**
   * @param {Puppeteer.Page} [targetPage=currPage]
   * @return {?Puppeteer.Dialog} If present, the {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-dialog|Dialog} displayed in the page passed as parameter.
   */
  this.dialog = (targetPage) => {
    const page = typeof targetPage !== 'undefined' ? targetPage : currPage;
    if (!pagesToDialog.has(page)) {
      return null;
    }
    const dialog = pagesToDialog.get(page);
    if (dialog._handled === true) {
      pagesToDialog.delete(page);
      return null;
    }
    return dialog;
  };

  /**
   * Defines and returns the network cache status
   * @param {boolean} [enabled] if passed, network cache will be enabled/disabled.
   * @return {Promise<boolean>} Promise which resolves to network cache status.
   */
  this.networkCache = async (enabled) => {
    if (typeof enabled === 'boolean') {
      networkCache = enabled;
    }
    const promises = [];
    const pages = Object.values(pagesIdMap);
    try {
      pages.forEach((page) => {
        promises.push(page.setCacheEnabled(networkCache));
      });
      await Promise.all(promises);
    } catch (error) {
      logger.error('networkCache error:', error);
    }
    return networkCache;
  };

  /**
   * Defines and returns the current network conditions
   * @param {NetworkConditionsConf} [options] if passed, custom network conditions will be applied.
   * @return {Promise<NetworkConditionsStatus>} Promise which resolves to network conditions applied.
   */
  this.networkConditions = async (options) => {
    if (typeof options === 'object') {
      if (options.offline !== true) {
        networkConditions.offline = false;
        networkConditions.latency = typeof options.latency === 'number' && options.latency > 0 ? parseInt(options.latency, 10) : 0;
        networkConditions.downloadThroughput = typeof options.downloadThroughput === 'number' ? parseInt(options.downloadThroughput, 10) : -1;
        networkConditions.uploadThroughput = typeof options.uploadThroughput === 'number' ? parseInt(options.uploadThroughput, 10) : -1;
        if (networkConditions.downloadThroughput < -1) {
          networkConditions.downloadThroughput = -1;
        }
        if (networkConditions.uploadThroughput < -1) {
          networkConditions.uploadThroughput = -1;
        }
      } else {
        networkConditions.offline = true;
        networkConditions.latency = 0;
        networkConditions.downloadThroughput = -1;
        networkConditions.uploadThroughput = -1;
      }
      const promises = [];
      const pages = Object.values(pagesIdMap);
      try {
        pages.forEach((page) => {
          promises.push(page._client.send('Network.emulateNetworkConditions', networkConditions));
        });
        await Promise.all(promises);
      } catch (error) {
        logger.error('networkConditions error:', error.message);
      }
    }
    return networkConditions;
  };

  /**
   * Removes any custom network condition
   * @return {Promise}
   */
  this.resetNetworkConditions = async () => this.networkConditions({ offline: false });

  /**
   * @return {GeolocationStatus}
   */
  this.geolocation = () => geolocation;

  /**
   * Defines gps coordinates and enables geolocation for all frames in workspace
   * @param {number} latitude
   * @param {number} longitude
   * @param {number} [accuracy=20]
   * @return {Promise}
   */
  this.updateGeolocation = async (latitude, longitude, accuracy = 20) => {
    if (typeof latitude !== 'number' || typeof longitude !== 'number' || typeof accuracy !== 'number') {
      return;
    }
    geolocation.enabled = true;
    geolocation.accuracy = accuracy;
    geolocation.latitude = latitude;
    geolocation.longitude = longitude;
    await _setGeolocation(Object.values(framesIdMap));
  };

  /**
   * Disables geolocation for all frames in workspace
   * @return {Promise}
   */
  this.disableGeolocation = async () => {
    geolocation.enabled = false;
    geolocation.accuracy = 20;
    geolocation.latitude = null;
    geolocation.longitude = null;
    await _setGeolocation(Object.values(framesIdMap));
  };

  /**
   * Inject a local javascript file in the current frame
   * @param {string} path The path of the javascript file to inject.
   * @param {string} [encoding=utf8] The encoding to use for reading the file.
   * @return {Promise<boolean>} true if file has been propelry injected, false otherwise.
   */
  this.injectScript = async (_file, _encoding) => {
    let file = path.normalize(_file);
    if (!path.isAbsolute(file)) {
      file = path.resolve(process.cwd(), file);
    }
    if (typeof onInitScripts.entries[file] !== 'undefined') {
      await currFrame.evaluate(onInitScripts.entries[file]).catch((error) => {
        logger.error('injectScript error:', error.message);
      });
      return true;
    } else if (typeof onLoadScripts.entries[file] !== 'undefined') {
      await currFrame.evaluate(onLoadScripts.entries[file]).catch((error) => {
        logger.error('injectScript error:', error.message);
      });
      return true;
    }
    const stat = fs.statSync(file);
    if (!stat.isFile(file)) {
      logger.error('injectScript:', `${file} is NOT a file.`);
      return false;
    }
    const encoding = _encoding || 'utf8';
    await currFrame.evaluate(fs.readFileSync(file, encoding)).catch((error) => {
      logger.error('injectScript error:', error.message);
    });
    return true;
  };

  /**
   * @return {Array<string>} All javascript files paths to inject when a frame is initialized.
   */
  this.onInitScripts = () => {
    const callback = value => Object.keys(defaults.onInitScripts).indexOf(value) === -1;
    return Object.keys(onInitScripts.entries).filter(callback);
  };

  /**
   * Adds a local javascript file to inject when a frame is initialized.
   * The frames already initialized will be not affected.
   * @param {string} path The path of the javascript file to inject.
   * @param {string} [encoding=utf8] The encoding to use for reading the file.
   * @return {boolean} true if file has been properly added, false otherwise.
   */
  this.addOnInitScript = (_file, _encoding) => {
    let file = path.normalize(_file);
    if (!path.isAbsolute(file)) {
      file = path.resolve(process.cwd(), file);
    }
    const stat = fs.statSync(file);
    if (!stat.isFile(file)) {
      logger.error('addOnInitScript:', `${file} is NOT a file.`);
      return false;
    }
    if (typeof onInitScripts.entries[file] !== 'undefined') {
      logger.error('addOnInitScript:', `${file} is already added.`);
      return false;
    }
    const encoding = _encoding || 'utf8';
    onInitScripts.entries[file] = fs.readFileSync(file, encoding);
    onInitScripts.string += `\n\n${onInitScripts.entries[file]}`;
    return true;
  };

  /**
   * Removes a local javascript file from the pool of files to inject at the frame initialization.
   * The frames already initialized will be not affected.
   * @param {string} path The path of the javascript file to inject.
   * @return {boolean} true if file has been properly removed from pool, false otherwise.
   */
  this.removeOnInitScript = (_file) => {
    let file = path.normalize(_file);
    if (!path.isAbsolute(file)) {
      file = path.resolve(process.cwd(), file);
    }
    if (typeof onInitScripts.entries[file] === 'undefined') {
      logger.error('addOnInitScript:', `${file} is NOT in stack.`);
      return false;
    }
    delete onInitScripts.entries[file];
    onInitScripts.string = Object.values(onInitScripts.entries).join('\n\n');
    return true;
  };

  /**
   * Removes all local javascript files from the pool of files to inject at the frame initialization.
   * The frames already initialized will be not affected.
   */
  this.flushOnInitScripts = () => {
    onInitScripts = JSON.parse(JSON.stringify(defaults.onInitScripts));
    onInitScripts.string = Object.values(onInitScripts.entries).join('\n\n');
  };

  /**
   * @return {Array<string>} All javascript files paths to inject when a frame is loaded.
   */
  this.onLoadScripts = () => {
    const callback = value => Object.keys(defaults.onLoadScripts).indexOf(value) === -1;
    return Object.keys(onLoadScripts.entries).filter(callback);
  };

  /**
   * Adds a local javascript file to inject when a frame is loaded.
   * The frames already loaded will be not affected.
   * @param {string} path The path of the javascript file to inject.
   * @param {string} [encoding=utf8] The encoding to use for reading the file.
   * @return {boolean} true if file has been properly added, false otherwise.
   */
  this.addOnLoadScript = async (_file, _encoding) => {
    let file = path.normalize(_file);
    if (!path.isAbsolute(file)) {
      file = path.resolve(process.cwd(), file);
    }
    const stat = fs.statSync(file);
    if (!stat.isFile()) {
      logger.error('addOnLoadScript:', `${file} is NOT a file.`);
      return false;
    }
    if (typeof onLoadScripts.entries[file] !== 'undefined') {
      logger.error('addOnLoadScript:', `${file} is already added.`);
      return false;
    }
    const encoding = _encoding || 'utf8';
    onLoadScripts.entries[file] = fs.readFileSync(file, encoding);
    onLoadScripts.string += `\n\n${onLoadScripts.entries[file]}`;
    return true;
  };

  /**
   * Removes a local javascript file from the pool of files to inject when a frame is loaded.
   * The frames already loaded will be not affected.
   * @param {string} path The path of the javascript file to inject.
   * @return {boolean} true if file has been properly removed from pool, false otherwise.
   */
  this.removeOnLoadScript = (_file) => {
    let file = path.normalize(_file);
    if (!path.isAbsolute(file)) {
      file = path.resolve(process.cwd(), file);
    }
    if (typeof onLoadScripts.entries[file] === 'undefined') {
      logger.error('onLoadScripts:', `${file} is NOT in stack.`);
      return false;
    }
    delete onLoadScripts.entries[file];
    onLoadScripts.string = Object.values(onLoadScripts.entries).join('\n\n');
    return true;
  };

  /**
   * Removes all local javascript files from the pool of files to inject when a frame is loaded.
   * The frames already loaded will be not affected.
   */
  this.flushOnLoadScripts = () => {
    onLoadScripts = JSON.parse(JSON.stringify(defaults.onLoadScripts));
    onLoadScripts.string = Object.values(onLoadScripts.entries).join('\n\n');
  };

  /**
   * @return {WorkspaceWatcher}
   */
  this.watcher = () => watcher;

  /**
   * @return {ResourcesManager}
   */
  this.resourcesManager = () => resourcesManager;

  /**
   * @return {Registry}
   */
  this.registry = () => registry;

  /**
   * @return {Puppeteer.Page} Puppeteer's instance of a new page that will be not a part of workspace.
   */
  this.newSilentPage = async () => {
    internalPageCreation = true;
    const silentPage = await browser.newPage();
    internalPageCreation = false;
    return silentPage;
  };

  /**
   * Defines and returns if the navigation is locked.
   * @param {boolean} [flag] Lock the navigation if true.
   * @return {boolean} If true the navigation will be prevented, when happens.
   */
  this.navigationLocked = (flag) => {
    if (typeof flag === 'boolean') {
      isNavigationLocked = flag;
    }
    return flag;
  };

  /**
   * Set the html to return when the navigation happens.
   * @param {?string} html If null, navigation will occur normally.
   */
  this.navigationHTML = (html) => {
    if (html === null || typeof html === 'string') {
      isNavigationLocked = html !== null;
      navigationHTML = html;
    }
    return navigationHTML;
  };

  /**
   * Force the page stop all navigations and pending resource fetches.
   * @return {Promise}
   */
  this.stopLoading = async () => {
    await currPage._client.send('Page.stopLoading').catch((error) => {
      logger.error('stopLoading error:', error.message);
    });
    const pageFrames = Object.values(framesIdMap).filter(frame => framesToPageMap.get(frame) === currPage);
    await _checkInjections(pageFrames);
  };

  return _init();
}

inherits(Workspace, EventEmitter);

/**
 * This event is emitted when the workspace is propely initialized, onInitScripts are injected
 * and internal functions are exposed. It is listened by Surfer class.
 * @event Workspace#ready
 */

/**
 * This event is emitted when a new page is created, for example by the execution of a window.open.
 * Internally, it is listened by WorkspaceWatcher class.
 * @event Workspace#page-created
 * @param {Puppeteer.Page} page Instance of Puppeter's page just created. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
 * @param {string} pageId The id of the page just created.
 */

/**
 * This event is emitted when the current page navigates to a new url.
 * Internally, it is listened by WorkspaceWatcher class.
 * @event Workspace#page-navigated
 * @param {Puppeteer.Page} page Instance of Puppeter's page. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
 * @param {string} pageId The id of the page.
 */

/**
 * This event is emitted when the current page is changed and the current context is updated.
 * Internally, it is listened by WorkspaceWatcher class.
 * @event Workspace#page-changed
 * @param {Puppeteer.Page} page Instance of Puppeter's page. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
 * @param {string} pageId The id of the page.
 */

/**
 * This event is emitted when a page is destroyed. If this page is also the current page,
 * the current context will be updated, the mainPage will become the current page and a
 * page-changed event will occur.
 * Internally, it is listened by WorkspaceWatcher class.
 * @event Workspace#page-destroyed
 * @param {Puppeteer.Page} page Instance of Puppeter's page. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page|Page}.
 * @param {string} pageId The id of the page.
 */

/**
 * This event is emitted when a dialog appears in the current page.
 * Internally, it is listened by WorkspaceWatcher class.
 * @event Workspace#dialog
 * @param {Puppeteer.Dialog} dialog Instance of Puppeter's dialog. See {@link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-dialog|Dialog}.
 */

/**
 * This event is emitted when a resource is fully processed by DOM.
 * Internally, it is listened by ResourceMonitor.
 * @event Workspace#resource-timing-processed
 * @param {DOMTiming} timing
 * @param {Puppeteer.Frame}
 * @param {Puppeteer.Pafe}
 */

/**
 * @typedef {Object} NetworkConditionsStatus
 * @property {boolean} offline Describes if network is enabled.
 * @property {number} latency Minimum latency from request sent to response headers received (ms)
 * @property {number} downloadThroughput Maximal aggregated download throughput (bytes/sec). -1 no download throttling.
 * @property {number} uploadThroughput Maximal aggregated upload throughput (bytes/sec). -1 no upload throttling.
 */

/**
 * @typedef {Object} GeolocationStatus
 * @property {number} latitude The latitude
 * @property {number} longitude The longitude
 * @property {number} accuracy The accuracy in meters
 * @property {boolean} enabled Describes if the geolocation is enabled
 */

/**
 * @typedef {Object} HTMLSnapshotConf
 * @property {string} replacedNodeName Custom node name used when a node is replaced.
 * @property {string} replacedAttrName Name of the attribute where will be stored the orginal node name (if replaced).
 * @property {string} dataUrlAttrName Name of the attribute where will be stored the base64 of canvas elements.
 * @property {boolean} internalScript Describes if the internal javascript should be added in the snapshot.
 * @property {boolean} removeBase Describes if any base element should be removed from html snapshot.
 * @property {string} scriptsToKeepSelector Describes the selector for script/noscript elements to keep in snapshot.
 */

/**
 * @typedef {Object} DOMTiming
 * @property {string} url Url of requested resource.
 * @property {number} startTime Timestamp for the time a resource fetch started.
 * @property {number} fetchStart Milliseconds passed since startTime before the browser starts to fetch
 * the resource. 0 if not happened.
 * @property {number} domainLookupStart Milliseconds passed since startTime before the browser starts
 * the domain name lookup for the resource. 0 if not happened.
 * @property {number} domainLookupEnd Milliseconds passed since startTime after the browser finishes
 * the domain name lookup for the resource. 0 if not happened.
 * @property {number} connectStart Milliseconds passed since startTime before the browser starts to establish
 * the connection to the server to retrieve the resource. 0 if not happened.
 * @property {number} secureConnectionStart Milliseconds passed since startTime before the browser starts
 * the handshake process to secure the current connection. 0 if not happened.
 * @property {number} connectEnd Milliseconds passed since startTime after the browser finishes establishing
 * the connection to the server to retrieve the resource. 0 if not happened.
 * @property {number} requestStart Milliseconds passed since startTime before the browser starts requesting
 * the resource from the server. 0 if not happened.
 * @property {number} responseStart Milliseconds passed since startTime after the browser receives
 * the first byte of the response from the server. 0 if not happened.
 * @property {number} responseEnd Milliseconds passed since startTime after the browser receives the last byte of
 * the resource or immediately before the transport connection is closed, whichever comes first. 0 if not happened.
 * @property {number} size Size (in octets) received from the fetch (HTTP or cache) of the message body,
 * after removing any applied content-codings. 0 if no response.
 */

/**
 * @typedef {Object} HistoryStatus
 * @property {boolean} canGoBack Describes if the current page can go back in history
 * @property {boolean} canGoForward Describes if the current page can go forward in history
 */

module.exports = Workspace;
