const { inherits } = require('util');
const EventEmitter = require('events');
const logger = require('./Logger.js').client('CustomMonitor');

/**
 * CustomMonitor class is used by WorkspaceWatcher in order to understand if a page is successfully loaded
 * by using custom check callbacks. It should be used for custom scenarios where the standard behaviour
 * of the framework is not able to trigger the loaded event in the right moment.
 * When the monitor is running, it will execute all custom check callbacks everytime an amount of time is passed,
 * when all callbacks will return true the loaded event will be triggered.
 * @constructor
 * @param {Workspace} workspace
 * @emits CustomMonitor#loaded
 */
function CustomMonitor(_workspace) {
  const self = this;

  const workspace = _workspace;

  const TRESHOLDS = {
    checkInterval: 500,
    maxChecks: 10,
  };

  let isRunning = false;

  let isLoaded = false;

  let checksCount = 0;

  let checksTimer = null;

  let checkLoadedCallbacks = [];

  let checkLoadedCallbacksCount = 0;

  async function _triggerCallbacks() {
    checksTimer = clearTimeout(checksTimer);
    if (workspace.dialog() !== null) {
      self.stop();
      isLoaded = true;
      logger.warning('loaded per dialog');
      self.emit('loaded');
      return;
    }
    if (!checkLoadedCallbacksCount || checksCount >= TRESHOLDS.maxChecks) {
      self.stop();
      isLoaded = true;
      if (checkLoadedCallbacksCount > 0) {
        logger.warning('loaded per max checks');
      } else {
        logger.debug('loaded per no callbacks');
      }
      self.emit('loaded');
      return;
    }
    const allLoaded = await _executeCallbacks();
    if (allLoaded) {
      self.stop();
      isLoaded = true;
      logger.info('loaded');
      self.emit('loaded');
      return;
    }
    checksCount++;
    checksTimer = setTimeout(_triggerCallbacks, TRESHOLDS.checkInterval);
  }

  async function _executeCallbacks(_callbackIndex) {
    const callbackIndex = typeof _callbackIndex !== 'number' ? 0 : _callbackIndex;
    if (callbackIndex > (checkLoadedCallbacksCount - 1)) {
      return true;
    }
    const allLoaded = await checkLoadedCallbacks[callbackIndex](workspace);
    if (!allLoaded) {
      return false;
    }
    return _executeCallbacks(callbackIndex + 1);
  }

  /**
   * Adds a custom check callback in the stack.
   * @param {function} callback Function that should return true if it consider that the page is loaded, false otherwise.
   * During the execution, the workspace will be passed as parameter.
   * @return {boolean} true if the callback is added in the stack, false otherwise.
   */
  this.addCheckLoadedCallback = (callback) => {
    if (checkLoadedCallbacks.indexOf(callback) === -1) {
      checkLoadedCallbacks.push(callback);
      checkLoadedCallbacksCount++;
      return true;
    }
    return false;
  };

  /**
   * Removes a custom check callback from the stack.
   * @param {function} callback Function to remove from the stack.
   * @return {boolean} true if the callback is removed from the stack, false otherwise.
   */
  this.removeCheckLoadedCallback = (callback) => {
    const callbackIndex = checkLoadedCallbacks.indexOf(callback);
    if (callbackIndex > -1) {
      checkLoadedCallbacks.splice(callbackIndex, 1);
      checkLoadedCallbacksCount--;
      return true;
    }
    return false;
  };

  /**
   * Removes all custom checks callbacks from the stack.
   */
  this.flushCheckLoadedCallbacks = () => {
    checkLoadedCallbacks = [];
    checkLoadedCallbacksCount = 0;
    return true;
  };

  /**
   * Starts the monitoring
   */
  this.start = () => {
    if (isRunning) {
      self.stop();
    }
    isRunning = true;
    isLoaded = false;
    checksCount = 0;
    checksTimer = setTimeout(_triggerCallbacks, TRESHOLDS.checkInterval);
    logger.debug('started');
    return true;
  };

  /**
   * Stops the monitoring
   */
  this.stop = () => {
    checksTimer = clearTimeout(checksTimer);
    if (!isRunning) {
      logger.warning('stop not possible, monitor is already stopped.', 'verbose');
      return false;
    }
    isRunning = false;
    logger.debug('stopped');
    return true;
  };

  /**
   * Pauses the monitoring
   */
  this.pause = () => self.stop;

  /**
   * Resumes the monitoring
   */
  this.resume = () => self.start;

  /**
   * Returns if the monitoring is in progress
   * @return {boolean}
   */
  this.isRunning = () => isRunning;

  /**
   * Returns if the monitoring is and completed
   * @return {boolean}
   */
  this.isLoaded = () => isLoaded;

  /**
   * Defines and returns the current configuration
   * @param {CustomMonitorConf} [conf] If passed, the configuration will be updated.
   * @return {CustomMonitorConf}
   */
  this.configuration = (conf) => {
    if (typeof conf === 'object') {
      if (typeof conf.checkInterval === 'number') {
        TRESHOLDS.checkInterval = parseInt(conf.checkInterval, 10);
      }
      if (typeof conf.maxChecks === 'number') {
        TRESHOLDS.maxChecks = parseInt(conf.maxChecks, 10);
      }
    }
    return {
      checkInterval: TRESHOLDS.checkInterval,
      maxChecks: TRESHOLDS.maxChecks,
    };
  };

  return self;
}

inherits(CustomMonitor, EventEmitter);

/**
 * This event is emitted when the monitoring is started and completed properly,
 * it means that all callbacks in the stack have been executed and have returned true.
 * @event CustomMonitor#loaded
 */

/**
 * @typedef {Object} CustomMonitorConf
 * @property {number} [checkInterval] The amount of time, in milliseconds, to pass between each execution of the loaded checker. Defaults to 500.
 * @property {number} [maxChecks] The amount of checks before the monitor will be considered as loaded anyway. Defaults to 10.
 */

module.exports = CustomMonitor;
