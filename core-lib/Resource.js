/**
 * Resource class is used by ResourcesMonitor and ResourcesManager in order to describe
 * any resource requested during the execution
 * @constructor
 * @param {Puppeteer.Request|Puppeteer.Response} [internalResource]
 * @param {?SimpleTiming|boolean} [timing] null in case of about:blank resource
 * @param {ResourcesManager} [manager]
 */
function Resource(_internalResource, _timing, _manager) {
  const self = this;
  const internalResource = _internalResource;
  const manager = _manager;
  const urlObj = {
    full: 'about:blank',
    protocol: '',
    domain: 'about:blank',
    path: '',
    query: '',
    hash: '',
  };
  const validAppContentTypes = ['application/javascript', 'application/json'];
  const validAppContentTypesCount = validAppContentTypes.length;
  let id = null;
  let timing = _timing;
  let isBlacklisted = false;
  let isPing = false;
  let recognizedAs = '';
  let request = null;
  let response = null;
  let status = -1;
  let statusText = null;
  let method = null;
  let isRedirect = false;
  let isError = false;
  let contentType = null;
  let isBinary = false;
  let isXML = false;
  let size = null;

  function _generateUrlObj(_url) {
    let url = _url;
    if (typeof url !== 'string') {
      return;
    }
    urlObj.full = url;
    // Calculating hash (if needed)
    const hIndex = url.indexOf('#');
    if (hIndex > -1) {
      urlObj.hash = url.substring(hIndex + 1);
      url = url.substring(0, hIndex);
    }
    // Calculating query (if needed)
    const qIndex = url.indexOf('?');
    if (qIndex > -1) {
      urlObj.query = url.substring(qIndex + 1);
      url = url.substring(0, qIndex);
    }
    // Calculating protocol and domain
    const pIndex = url.indexOf('://');
    if (pIndex === -1) {
      urlObj.full = '';
      urlObj.hash = '';
      urlObj.query = '';
      return;
    }
    urlObj.protocol = url.substring(0, pIndex);
    urlObj.domain = url.substring(pIndex + 3);
    // Calculating path (if needed)
    const sIndex = urlObj.domain.indexOf('/');
    if (sIndex > -1) {
      urlObj.path = urlObj.domain.substring(sIndex);
      urlObj.domain = urlObj.domain.substring(0, sIndex);
    } else {
      urlObj.path = '';
    }
  }

  function _init() {
    if (internalResource) {
      if (typeof internalResource.request !== 'undefined') {
        response = internalResource;
        request = internalResource.request();
      } else {
        request = internalResource;
        response = internalResource.response();
      }
      id = request._interceptionId;
      urlObj.full = '';
      urlObj.domain = '';
      if (response !== null) {
        _generateUrlObj(response.url());
        status = response.status();
        statusText = response.statusText();
        isRedirect = status >= 300 && status <= 399;
        isError = !isRedirect && !response.ok();
        isBinary = response._forcedAsBinary === true;
        const contentTypeHeader = response.headers()['content-type'];
        if (contentTypeHeader) {
          contentType = contentTypeHeader;
        }
      } else {
        _generateUrlObj(request.url());
        isError = request.failure() !== null;
        isBinary = false;
      }
      method = request.method();
      if (!contentType) {
        contentType = request.resourceType();
      }
      if (contentType.indexOf('application/xml') === 0) {
        isBinary = true;
        isXML = true;
      } else if (contentType.indexOf('application') === 0) {
        isBinary = true;
        for (let i = 0; i < validAppContentTypesCount && isBinary; i++) {
          if (contentType.indexOf(validAppContentTypes[i]) === 0) {
            isBinary = false;
          }
        }
      }
    }
    if (manager) {
      isBlacklisted = manager.isBlacklisted(self);
      isPing = manager.isPing(self);
      recognizedAs = manager.recognizedAs(self);
    }
    return self;
  }

  /**
   * Returns the internal Puppeteer's instance that has generated the resource
   * @return {Puppeteer.Request|Puppeteer.Response}
   */
  this.internal = () => internalResource;

  /**
   * Returns the id assigned to the request performed by Puppeteer
   * It could be null for about:blank resources
   * @return {?string}
   */
  this.id = () => id;

  /**
   * Returns the complete url
   * @return {string}
   */
  this.url = () => urlObj.full;

  /**
   * Returns the protocol
   * @return {string}
   */
  this.protocol = () => urlObj.protocol;

  /**
   * Returns the domain
   * @return {string}
   */
  this.domain = () => urlObj.domain;

  /**
   * Returns the path
   * @return {string}
   */
  this.path = () => urlObj.path;

  /**
   * Returns the query string
   * @return {string}
   */
  this.query = () => urlObj.query;

  /**
   * Returns the hash
   * @return {string}
   */
  this.hash = () => urlObj.hash;

  /**
   * Sets and returns the timing assigned to the resource
   * @param {SimpleTiming|DOMTiming} resourceTiming
   * @return {?ResourceTiming}
   */
  this.timing = (resourceTiming) => {
    if (typeof resourceTiming !== 'undefined') {
      let startTime = null;
      if (timing) {
        ({ startTime } = timing);
      }
      let endTime = null;
      if (timing) {
        ({ endTime } = timing);
      }
      timing = resourceTiming;
      size = {
        decoded: timing.size,
        encoded: timing.sizeEncoded > 0 && timing.size !== timing.sizeEncoded ? timing.sizeEncoded : 0,
      };
      delete timing.size;
      delete timing.url;
      if (startTime !== null && endTime !== null) {
        if (timing.startTime !== startTime) {
          timing.internalStart = startTime;
        }
        timing.endTime = endTime;
      }
    }
    return timing;
  };

  /**
   * Returns if the resource is blacklisted, request has been aborted.
   * @return {boolean}
   */
  this.isBlacklisted = () => isBlacklisted;

  /**
   * Returns if the resource is considered as a ping. It means the resource has reached the threshold in ResourcesMonitor.
   * This threshold defines the maximum number of times an equal request can be performed without to be considered as a ping.
   * @return {boolean}
   */
  this.isPing = () => isPing;

  /**
   * Returns the name of the recognizer triggered by ResourcesManager. Empty string if no recognizer has been triggered.
   * @return {string}
   */
  this.recognizedAs = () => recognizedAs;

  /**
   * Returns the response status. -1 if no response.
   * @return {number}
   */
  this.status = () => status;

  /**
   * Returns the response status text returned by server. null if no response.
   * @return {?string}
   */
  this.statusText = () => statusText;

  /**
   * Returns if the resource is considered as an error, no response or (status < 200 || status > 399)
   * @return {boolean}
   */
  this.isError = () => isError;

  /**
   * Returns an human readable string that describes the error. null if the resource isn't considered as an error.
   * @return {?string}
   */
  this.failure = () => {
    if (!isError) {
      return null;
    }
    return response !== null ? statusText : request.failure().errorText;
  };

  /**
   * Returns the decoded data size. -1 if no response.
   * @return {number}
   */
  this.size = () => {
    if (size !== null) {
      return size.decoded;
    }
    return response !== null ? response.size() : -1;
  };

  /**
   * Returns the encoded data size. -1 if no response. 0 if response is not encoded.
   * @return {number}
   */
  this.sizeEncoded = () => {
    if (size !== null) {
      return size.encoded;
    }
    return response !== null ? response.sizeEncoded() : -1;
  };

  /**
   * Returns the method used in order to perform the request.
   * @return {string}
   */
  this.method = () => method;

  /**
   * Returns if the resource is a redirect
   * @return {boolean}
   */
  this.isRedirect = () => isRedirect;

  /**
   * Returns if the resource has been downloaded form the server.
   * @return {boolean}
   */
  this.hasResponded = () => response !== null;

  /**
   * Returns if the content type of the resource. null if no response.
   * @return {?string}
   */
  this.contentType = () => contentType;

  /**
   * Returns if the resource is cached, no network call.
   * @return {boolean}
   */
  this.isCached = () => response !== null && response.fromCache();

  /**
   * Returns the Puppeteer.Request assigned to the resource. null if no request.
   * @return {?Puppeteer.Request}
   */
  this.request = () => request;

  /**
   * Returns the Puppeteer.Response assigned to the resource. null if no response.
   * @return {?Puppeteer.Response}
   */
  this.response = () => response;

  /**
   * TODO: Would be better to implement isHTML method and to stop to use this one.
   * Returns if the resource is considered as a binary file
   */
  this.isBinary = () => isBinary;

  /**
   * Returns if the resource is considered as an xml document
   */
  this.isXML = () => isXML;

  return _init();
}

/**
 * @typedef {Object} ResourceTiming
 * @property {number} startTime Timestamp for the time a resource fetch started.
 * @property {number} endTime Timestamp for the time a resource has finished to be processed.
 * @property {number} [internalStart] Timestamp for the time a resource has been acknowledged to be requested (internal use).
 * @property {number} [fetchStart] Milliseconds passed since startTime before the browser starts to fetch
 * the resource. 0 if not happened.
 * @property {number} [domainLookupStart] Milliseconds passed since startTime before the browser starts
 * the domain name lookup for the resource. 0 if not happened.
 * @property {number} [domainLookupEnd] Milliseconds passed since startTime after the browser finishes
 * the domain name lookup for the resource. 0 if not happened.
 * @property {number} [connectStart] Milliseconds passed since startTime before the browser starts to establish
 * the connection to the server to retrieve the resource. 0 if not happened.
 * @property {number} [secureConnectionStart] Milliseconds passed since startTime before the browser starts
 * the handshake process to secure the current connection. 0 if not happened.
 * @property {number} [connectEnd] Milliseconds passed since startTime after the browser finishes establishing
 * the connection to the server to retrieve the resource. 0 if not happened.
 * @property {number} [requestStart] Milliseconds passed since startTime before the browser starts requesting
 * the resource from the server. 0 if not happened.
 * @property {number} [responseStart] Milliseconds passed since startTime after the browser receives
 * the first byte of the response from the server. 0 if not happened.
 * @property {number} [responseEnd] Milliseconds passed since startTime after the browser receives the last byte of
 * the resource or immediately before the transport connection is closed, whichever comes first. 0 if not happened.
 */

module.exports = Resource;
