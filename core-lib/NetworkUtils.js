const domainValidator = new RegExp('^' +
  '(?:' +
    // host name
    '(?:(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)' +
    // domain name
    '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)*' +
    // TLD identifier
    '(?:\\.(?:[a-z0-9\\u00a1-\\uffff]{2,}))' +
    // TLD may end with dot
    '\\.?' +
  '|' +
    '(?!(?:localhost))' +
    // host name
    '(?:(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)' +
  ')' +
'$', 'i');

const cookieDomainValidator = new RegExp('^\\.*' +
  '(?:' +
    // host name
    '(?:(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)' +
    // domain name
    '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)*' +
    // TLD identifier
    '(?:\\.(?:[a-z0-9\\u00a1-\\uffff]{2,}))' +
    // TLD may end with dot
    '\\.?' +
  '|' +
    '(?!(?:localhost))' +
    // host name
    '(?:(?:[a-z\\u00a1-\\uffff0-9]-*_*)*[a-z\\u00a1-\\uffff0-9]+)' +
  ')' +
'$', 'i');

const ipValidator = new RegExp('^' +
  // IP address exclusion
  // private & local networks
  '(?!(?:127)(?:\\.\\d{1,3}){3})' +
  // IP address dotted notation octets
  // excludes loopback network 0.0.0.0
  // excludes reserved space >= 224.0.0.0
  // excludes network & broacast addresses
  // (first & last IP address of each class)
  '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
  '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
  '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
'$', 'i');

/**
 * NetworkUtils provides methods to test strings that should be considered as network stuff.
 * @constructor
 */
class NetworkUtils {
  /**
   * Returns if a string can be considered as a valid domain. localhost is not allowed.
   * @param {string} domain
   * @return {boolean}
   */
  static testDomain(domain) {
    return typeof domain === 'string' && domainValidator.test(domain);
  }

  /**
   * Returns if a string can be considered as a valid domain to use in a cookie. localhost is not allowed.
   * @param {string} domain
   * @return {boolean}
   */
  static testCookieDomain(domain) {
    return typeof domain === 'string' && cookieDomainValidator.test(domain);
  }

  /**
   * Returns if a string can be considered as a valid ip address. Local network ips and reserved addresses are not allowed.
   * @param {string} ip
   * @return {boolean}
   */
  static testIp(ip) {
    return typeof ip === 'string' && ipValidator.test(ip);
  }
}

module.exports = NetworkUtils;
