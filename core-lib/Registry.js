/**
 * Registry class is used by Workspace in order to provide a place where store global variables.
 * @constructor
 */
function Registry() {
  let storage = {};

  /**
   * Sets a value in registry identified by key.
   * @param {string} key
   * @param {*} value
   * @return {boolean} true if stored, false otherwise.
   */
  this.set = (key, value) => {
    if (typeof key !== 'string' || !key) {
      return false;
    }
    storage[key] = value;
    return true;
  };

  /**
   * @param {string} key
   * @return {*} The value identified by the passed key.
   */
  this.get = (key) => {
    if (typeof key !== 'string' || !key || typeof storage[key] === 'undefined') {
      return undefined;
    }
    return storage[key];
  };

  /**
   * @param {string} key
   * @return {boolean} true if the passed key identifies a value, false otherwise.
   */
  this.has = key => typeof storage[key] !== 'undefined';

  /**
   * Removes the value identified by passed key.
   * @param {string} key
   * @return {boolean} true if removed, false otherwise.
   */
  this.delete = (key) => {
    if (typeof key !== 'string' || !key || typeof storage[key] === 'undefined') {
      return false;
    }
    delete storage[key];
    return true;
  };

  /**
   * The passed key will be linked to the value identified by the keyPath.
   * @param {string} key
   * @param {string} keyPath
   * @return {boolean} true if the link has been created successfully, false otherwise.
   */
  this.link = (key, keyPath) => {
    if (typeof key !== 'string' || !key) {
      return false;
    }
    if (typeof keyPath !== 'string' || !keyPath) {
      return false;
    }
    const parts = keyPath.split('.');
    const partsCount = parts.length;
    let errorFound = false;
    let currObj = storage;
    for (let i = 0; i < partsCount && !errorFound; i++) {
      const currKey = parts[i];
      if (currObj[currKey] !== 'undefined') {
        currObj = currObj[currKey];
      } else {
        errorFound = true;
      }
    }
    if (errorFound) {
      return false;
    }
    storage[key] = currObj;
    return true;
  };

  /**
   * Removes all values stored in registry
   */
  this.flush = () => {
    storage = {};
  };

  return this;
}

module.exports = Registry;
