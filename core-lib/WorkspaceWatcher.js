const { inherits } = require('util');
const EventEmitter = require('events');
const ResourcesMonitor = require('./ResourcesMonitor.js');
const CustomMonitor = require('./CustomMonitor.js');
const TimersMonitor = require('./TimersMonitor.js');
const logger = require('./Logger.js').client('WorkspaceWatcher');

/**
 * WorkspaceWatcher is used in order to understand when the workspace should be considered as loaded after the execution
 * of an action. It executes a sequence of monitors that observe diffent things. When a monitor triggers his loaded event
 * the next one will be executed, this process will continue until the chain is completed.
 * The monitor chain is composed as follows:
 * 1) ResourcesMonitor, it observes the network activity.
 * 2) CustomMonitor, it executes custom checks callbacks.
 * 3) TimersMonitor, it observes the timers to execute in the current frame.
 * If a network-activity-ignored event is triggered by ResourcesMonitor during the execution of CustomMonitor/TimersMonitor
 * the chain will be resetted and the observation process will be restarted, it can happens for a maximum number of times.
 * If the monitors chain is not completed after the maximum wait treshold the loaded event will be triggered anyway.
 * WorkspaceWatcher also observes the status of workspace and provides methods in order to understand:
 * - if the current frame has navigated during the observation
 * - if the current pages is changed during the observation
 * Any network-activity event triggered by ResourcesMonitor during the observation will be propaged as a resource event.
 * @constructor
 * @param {Workspace} workspace
 * @param {Puppeteer.Page} mainPage
 * @emits WorkspaceWatcher#loaded
 * @emits WorkspaceWatcher#resources-loaded
 * @emits WorkspaceWatcher#custom-loaded
 * @emits WorkspaceWatcher#timers-loaded
 * @emits WorkspaceWatcher#started
 * @emits WorkspaceWatcher#stopped
 * @emits WorkspaceWatcher#paused
 * @emits WorkspaceWatcher#resumed
 * @emits WorkspaceWatcher#resource-acknowledged
 * @emits WorkspaceWatcher#resource-discarded
 * @emits WorkspaceWatcher#resource-processed
 */
function WorkspaceWatcher(_workspace, _mainPage) {
  const self = this;

  const workspace = _workspace;

  const mainPage = _mainPage;

  const resourcesMonitors = new WeakMap();

  const customMonitor = new CustomMonitor(workspace);

  const timersMonitor = new TimersMonitor(workspace);

  const TRESHOLDS = {
    maxRestarts: 10,
    maxWait: 60,
  };

  let isRunning = false;

  let isPaused = false;

  let isLoaded = false;

  let hasNavigated = false;

  let hasNavigatedPerHash = false;

  let isPageChanged = false;

  let keepStatus = false;

  let restartsCount = 0;

  let startTime = -1;

  let resourcesStopTime = -1;

  let customChecksStopTime = -1;

  let stopTime = -1;

  let currPage = mainPage;

  let monitorsChain = [];

  let currMonitorIndex = -1;

  let watchdog = null;

  let loadedListeners = [];

  let loadedListenersCount = 0;

  let waitingForNewPage = null;

  let newPage = null;

  function _init() {
    resourcesMonitors.set(currPage, new ResourcesMonitor(workspace, currPage));
    workspace.addListener('waiting-new-page', _waitingNewPageListener);
    workspace.addListener('page-initialized', _pageInitializedListener);
    workspace.addListener('page-created', _pageCreatedListener);
    workspace.addListener('page-changed', _pageChangedListener);
    workspace.addListener('page-destroyed', _pageDestroyedListener);
    workspace.addListener('page-navigated', _pageNavigatedListener);
    workspace.addListener('dialog', _dialogListener);
    return self;
  }

  function _waitingNewPageListener(url) {
    waitingForNewPage = url;
    _restartMonitoring(true, true);
  }

  function _pageInitializedListener(page) {
    newPage = page;
  }

  function _pageCreatedListener(page) {
    const monitor = new ResourcesMonitor(workspace, page);
    monitor.configuration(resourcesMonitors.get(mainPage).configuration());
    resourcesMonitors.set(page, monitor);
  }

  function _pageChangedListener(page) {
    const monitor = resourcesMonitors.get(currPage);
    currPage = page;
    if (currPage === newPage) {
      newPage = null;
    }
    if (isRunning) {
      isPageChanged = true;
      hasNavigated = false;
      hasNavigatedPerHash = false;
      _detachMonitorListeners(monitor, true);
      _restartMonitoring(true);
    }
  }

  function _pageDestroyedListener(page) {
    const monitor = resourcesMonitors.get(page);
    if (isRunning) {
      monitor.stop();
      _detachMonitorListeners(monitor, true);
    }
    resourcesMonitors.delete(page);
  }

  function _pageNavigatedListener() {
    if (isPageChanged === true) {
      waitingForNewPage = null;
    }
    if (isRunning && !hasNavigated) {
      const mainResource = workspace.mainResource();
      hasNavigated = true;
      hasNavigatedPerHash = mainResource !== null && startTime > mainResource.timing().startTime;
    }
  }

  function _dialogListener() {
    if (isRunning) {
      self.stop();
      monitorsChain = [];
      currMonitorIndex = -1;
      isLoaded = true;
      logger.warning('everything loaded per dialog');
      self.emit('loaded', false);
      _triggerLoadedListeners();
    }
  }

  function _monitorLoadedCallback() {
    const loadedMonitor = monitorsChain[currMonitorIndex];
    if (loadedMonitor instanceof ResourcesMonitor) {
      resourcesStopTime = new Date().getTime();
      self.emit('resouces-loaded');
    } else if (loadedMonitor instanceof CustomMonitor) {
      customChecksStopTime = new Date().getTime();
      self.emit('custom-loaded');
    } else if (loadedMonitor instanceof TimersMonitor) {
      self.emit('timers-loaded');
    }
    if (currMonitorIndex === (monitorsChain.length - 1)) {
      if (waitingForNewPage !== null || newPage !== null) {
        /**
         * When a new page is created a chain of events will occur:
         * waiting-new-page (optional), page-initialized, page-created, page-changed.
         *
         * waitingForNewPage is set when waiting-new-page is emitted.
         * newPage is set when page-initialized is emitted.
         * waitingForNewPage is set to null when page-initialized is emitted.
         * newPage is set to null when page-changed is emitted.
         *
         * If waitingForNewPage is not null it means a new page will be initialized so the monitoring will be restarted
         * by observing only resources (fake observation) of current page, we don't need to check anything else since
         * a new page will be generated and observed. In this way we're decreasing the load, no evaluations are performed.
         * If newPage is not null it means a new page is creating so the monitoring will be restarted by observing it.
         * For this reason we have to not trigger the loaded event.
         */
        return;
      }
      self.stop();
      monitorsChain = [];
      currMonitorIndex = -1;
      isLoaded = true;
      logger.success('everything loaded');
      self.emit('loaded', false);
      _triggerLoadedListeners();
      return;
    }
    _detachMonitorListeners(loadedMonitor);
    currMonitorIndex++;
    _attachMonitorListener(monitorsChain[currMonitorIndex]);
    monitorsChain[currMonitorIndex].start();
  }

  function _detachMonitorListeners(monitor, noExtraListeners) {
    if (!monitor) {
      return;
    }
    monitor.removeAllListeners('loaded');
    if (monitor instanceof ResourcesMonitor) {
      monitor.removeAllListeners('network-activity-acknowledged');
      monitor.removeAllListeners('network-activity-discarded');
      monitor.removeAllListeners('network-activity-processed');
      if (!noExtraListeners) {
        monitor.addListener('network-activity-ignored', (resource) => {
          _emitResourceAcknowledged(resource);
          _restartMonitoring(false, true);
        });
      } else {
        monitor.removeAllListeners('network-activity-ignored');
      }
    }
  }

  function _attachMonitorListener(monitor) {
    monitor.addListener('loaded', _monitorLoadedCallback);
    if (monitor instanceof ResourcesMonitor) {
      monitor.addListener('network-activity-acknowledged', _emitResourceAcknowledged);
      monitor.addListener('network-activity-discarded', _emitResourceDiscarded);
      monitor.addListener('network-activity-processed', _emitResourceProcessed);
      monitor.removeAllListeners('network-activity-ignored');
    }
  }

  function _restartMonitoring(forced, keepIgnored = false) {
    if (!forced && restartsCount >= TRESHOLDS.maxRestarts) {
      logger.warning('max restarts reached');
      return;
    }
    if (!forced) {
      logger.debug('restart monitoring per network');
    }
    if (currMonitorIndex > -1) {
      const monitor = monitorsChain[currMonitorIndex];
      monitor.stop();
      _detachMonitorListeners(monitor, true);
    }
    const resourceMonitor = resourcesMonitors.get(currPage);
    monitorsChain = waitingForNewPage === null ? [resourceMonitor, customMonitor, timersMonitor] : [resourceMonitor];
    currMonitorIndex = 0;
    _attachMonitorListener(resourceMonitor);
    resourceMonitor.start(keepIgnored);
    if (!forced) {
      restartsCount++;
    }
  }

  async function _maxWaitReached() {
    self.stop();
    monitorsChain = [];
    currMonitorIndex = -1;
    isLoaded = true;
    await workspace.stopLoading();
    logger.warning('everything loaded per max wait');
    self.emit('loaded', true);
    _triggerLoadedListeners(true);
  }

  function _triggerLoadedListeners(forced = false) {
    for (let i = 0; i < loadedListenersCount; i++) {
      loadedListeners[i](workspace.mainResource(), forced);
    }
  }

  function _emitResourceAcknowledged(resource) {
    self.emit('resource-acknowledged', resource);
  }

  function _emitResourceDiscarded(resource) {
    self.emit('resource-discarded', resource);
  }

  function _emitResourceProcessed(resource) {
    self.emit('resource-processed', resource);
  }

  /**
   * Sets up a function that will be called whenever the watcher will consider the workspace as loaded.
   * @param {function} listener
   * @return {boolean} true if the function is set up, false otherwise.
   */
  this.addLoadedListener = (listener) => {
    if (loadedListeners.indexOf(listener) === -1) {
      loadedListeners.push(listener);
      loadedListenersCount++;
      return true;
    }
    return false;
  };

  /**
   * Removes a function previously registered with addLoadedListener.
   * @param {function} listener
   * @return {boolean} true if the function is removed, false otherwise.
   */
  this.removeLoadedListener = (listener) => {
    const listenerIndex = loadedListeners.indexOf(listener);
    if (listenerIndex > -1) {
      loadedListeners.splice(listenerIndex, 1);
      loadedListenersCount--;
      return true;
    }
    return false;
  };

  /**
   * Removes all functions previously registered with addLoadedListener.
   */
  this.flushLoadedListeners = () => {
    loadedListeners = [];
    loadedListenersCount = 0;
    return true;
  };

  /**
   * Adds a custom check callback.
   * @param {function} callback Function that should return true if it consider that the page is loaded, false otherwise.
   * During the execution, the workspace will be passed as parameter.
   * @return {boolean} true if the callback is added, false otherwise.
   */
  this.addCheckLoadedCallback = callback => customMonitor.addCheckLoadedCallback(callback);

  /**
   * Removes a custom check callback.
   * @param {function} callback Function to remove.
   * @return {boolean} true if the callback is removed, false otherwise.
   */
  this.removeCheckLoadedCallback = callback => customMonitor.removeCheckLoadedCallback(callback);

  /**
   * Removes all custom checks callbacks.
   */
  this.flushCheckLoadedCallbacks = () => customMonitor.flushCheckLoadedCallbacks();

  /**
   * Stops the workspace observation (if needed) and resets the status.
   * By executing this method the loaded event will be never triggered.
   */
  this.reset = () => {
    if (isRunning || isPaused) {
      this.stop();
    }
    isRunning = false;
    isPaused = false;
    isLoaded = false;
    hasNavigated = false;
    hasNavigatedPerHash = false;
    isPageChanged = false;
    restartsCount = 0;
    return true;
  };

  /**
   * Starts the workspace observation. When the observation starts the monitors chain is always resetted.
   * @return {boolean} true if the observation is started, false otherwise.
   */
  this.start = () => {
    if (isRunning) {
      logger.warning('start not possible, watcher is already started.', 'verbose');
      return false;
    }
    logger.info('started');
    isRunning = true;
    isPaused = false;
    isLoaded = false;
    if (!keepStatus) {
      hasNavigated = false;
      hasNavigatedPerHash = false;
      isPageChanged = false;
      startTime = new Date().getTime();
    }
    restartsCount = 0;
    resourcesStopTime = -1;
    customChecksStopTime = -1;
    stopTime = -1;
    _restartMonitoring(true);
    self.emit('started');
    watchdog = setTimeout(_maxWaitReached, TRESHOLDS.maxWait * 1000);
    return true;
  };

  /**
   * Stops the workspace observation. By executing this method the loaded event will be never triggered.
   * @return {boolean} true if the observation is stopped, false otherwise.
   */
  this.stop = () => {
    watchdog = clearTimeout(watchdog);
    resourcesMonitors.get(currPage).removeAllListeners('network-activity-ignored');
    if (!isRunning) {
      logger.warning('stop not possible, watcher is already stopped.', 'verbose');
      return false;
    }
    const monitor = monitorsChain[currMonitorIndex];
    monitor.stop();
    _detachMonitorListeners(monitor, true);
    isRunning = false;
    isPaused = false;
    stopTime = new Date().getTime();
    if (resourcesStopTime === -1) {
      resourcesStopTime = stopTime;
    }
    if (customChecksStopTime === -1) {
      customChecksStopTime = stopTime;
    }
    logger.info('stopped');
    self.emit('stopped');
    return true;
  };

  /**
   * Pauses the workspace observation, it prevents the trigger of the loaded event but the observation status is retained.
   * If ResourcesMonitor is the running monitor it will be simply paused otherwise the chain will be resetted too.
   * @return {boolean} true if the observations is paused, false otherwise.
   */
  this.pause = () => {
    if (!isRunning || isPaused) {
      logger.warning(`pause not possible, watcher is ${!isRunning ? 'stopped' : 'already paused'}.`, 'verbose');
      return false;
    }
    const monitor = monitorsChain[currMonitorIndex];
    if (monitor instanceof ResourcesMonitor) {
      monitor.pause();
      isPaused = true;
      logger.debug('paused');
      self.emit('paused');
      return true;
    }
    _restartMonitoring(true, true);
    monitorsChain[0].pause();
    isPaused = true;
    logger.debug('paused');
    self.emit('paused');
    return true;
  };

  /**
   * Resumes the workspace observation, the loaded event will be triggered after the execution of the monitors in the chain.
   * @return {boolean} true if the observation is resumed, false otherwise.
   */
  this.resume = () => {
    if (!isRunning || !isPaused) {
      logger.warning(`resume not possible, watcher is ${!isRunning ? 'stopped' : 'is not paused'}.`, 'verbose');
      return false;
    }
    logger.debug('resumed');
    monitorsChain[currMonitorIndex].resume();
    isPaused = false;
    self.emit('resumed');
    return true;
  };

  /**
   * Returns if the observation is in progress.
   * @return {boolean}
   */
  this.isRunning = () => isRunning;

  /**
   * Returns if the observation is paused.
   * @return {boolean}
   */
  this.isPaused = () => isPaused;

  /**
   * Returns if the observation is started and completed.
   * @return {boolean}
   */
  this.isLoaded = () => isLoaded;

  /**
   * Returns if the current frame has navigated during the observation.
   * @return {boolean}
   */
  this.hasNavigated = () => hasNavigated;

  /**
   * Returns if the current frame has navigated without to perform any network request during the observation.
   * @return {boolean}
   */
  this.hasNavigatedPerHash = () => hasNavigated && hasNavigatedPerHash;

  /**
   * Returns if the current frame has navigated by performing a network request during the observation.
   * @return {boolean}
   */
  this.hasNavigatedPerNetwork = () => hasNavigated && !hasNavigatedPerHash;

  /**
   * Returns if the current page is changed during the observation.
   * @return {boolean}
   */
  this.isPageChanged = () => isPageChanged;

  /**
   * Defines and returns if the observation status is kept when watcher starts the observation.
   * @param {boolean} [flag] If passed, it will be set.
   * @return {boolean}
   */
  this.keepStatus = (flag) => {
    if (typeof flag === 'boolean') {
      keepStatus = flag;
    }
    return keepStatus;
  };

  /**
   * Returns the timestamp for the time the observation is started.
   * @return {number}
   */
  this.startTime = () => startTime;

  /**
   * Returns the timestamp for the time the observation is stopped.
   * @return {number}
   */
  this.stopTime = () => stopTime;

  /**
   * Returns the timestamp for the time the network observation is stopped.
   * @return {number}
   */
  this.resourcesStopTime = () => resourcesStopTime;

  /**
   * Returns the timestamp for the time the custom observation is stopped.
   * @return {number}
   */
  this.customChecksStopTime = () => customChecksStopTime;

  /**
   * Defines and returns the current configuration
   * @param {WorkspaceWatcherConf} [conf] If passed, the configuration will be updated.
   * @return {WorkspaceWatcherConf}
   */
  this.configuration = (conf) => {
    if (typeof conf === 'object') {
      if (typeof conf.maxRestarts === 'number') {
        TRESHOLDS.maxRestarts = parseInt(conf.maxRestarts, 10);
      }
      if (typeof conf.maxWait === 'number') {
        TRESHOLDS.maxWait = parseInt(conf.maxWait, 10);
      }
      if (typeof conf.ResourcesMonitor === 'object') {
        const pages = workspace.pages();
        pages.forEach((page) => {
          if (resourcesMonitors.has(page)) {
            resourcesMonitors.get(page).configuration(conf.ResourcesMonitor);
          }
        });
      }
      if (typeof conf.CustomMonitor === 'object') {
        customMonitor.configuration(conf.CustomMonitor);
      }
      if (typeof conf.TimersMonitor === 'object') {
        timersMonitor.configuration(conf.TimersMonitor);
      }
    }
    return {
      maxRestarts: TRESHOLDS.maxRestarts,
      maxWait: TRESHOLDS.maxWait,
      ResourcesMonitor: resourcesMonitors.get(mainPage).configuration(),
      CustomMonitor: customMonitor.configuration(),
      TimersMonitor: timersMonitor.configuration(),
    };
  };

  return _init();
}

inherits(WorkspaceWatcher, EventEmitter);

/**
 * This event is emitted when the workspace observation is started and completed properly.
 * @event WorkspaceWatcher#loaded
 */

/**
 * This event is emitted when the network observation, performed by ResourcesMonitor, is started and completed properly.
 * @event WorkspaceWatcher#resources-loaded
 */

/**
 * This event is emitted when the custom observation, performed by CustomMonitor, is started and completed properly.
 * @event WorkspaceWatcher#custom-loaded
 */

/**
 * This event is emitted when the workspace observation, performed by TimersMonitor, is started and completed properly.
 * @event WorkspaceWatcher#timers-loaded
 */

/**
 * This event is emitted when the workspace observation is started.
 * @event WorkspaceWatcher#started
 */

/**
 * This event is emitted when the workspace observation is stopped.
 * @event WorkspaceWatcher#stopped
 */

/**
 * This event is emitted when the workspace observation is paused.
 * @event WorkspaceWatcher#paused
 */

/**
 * This event is emitted when the workspace observation is resumed.
 * @event WorkspaceWatcher#resumed
 */

/**
 * This event is emitted when a resource has been intercepted, customized (if needed) and allowed to be requested.
 * @event WorkspaceWatcher#resource-acknowledged
 * @param {Resource}
 */

/**
 * This event is emitted when a resource has been intercepted and aborted.
 * @event WorkspaceWatcher#resource-discarded
 * @param {Resource}
 */

/**
 * This event is emitted when a resource has been requested and downloaded.
 * @event WorkspaceWatcher#resource-processed
 * @param {Resource}
 */

/**
 * @typedef {Object} WorkspaceWatcherConf
 * @property {number} [maxRestarts] The amount of time, in milliseconds, to pass between each execution of the loaded checker. Defaults to 10.
 * @property {number} [maxWait] The amount of checks before the monitor will be considered as loaded anyway. Defaults to 60.
 * @property {ResourcesMonitorConf} [ResourcesMonitor] The configuration for the resources monitor.
 * @property {CustomMonitorConf} [CustomMonitor] The configuration for the custom monitor.
 * @property {TimersMonitorConf} [TimersMonitor] The configuration for the timers monitor.
 */

module.exports = WorkspaceWatcher;
