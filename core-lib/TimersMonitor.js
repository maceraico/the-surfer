const { inherits } = require('util');
const EventEmitter = require('events');
const logger = require('./Logger.js').client('TimersMonitor');

/**
 * TimersMonitor class is used by WorkspaceWatcher in order to understand if a page is successfully loaded
 * by observing the timers to execute in the current frame.
 * When the monitor is running, it will count the timers everytime an amount of time is passed,
 * when the count will be 0 or the max checks treshold is reached the loaded event will be triggered.
 * This monitor considers timers:
 * - javascript timeouts
 * - javascript intervals
 * - refresh metas
 * @constructor
 * @param {Workspace} workspace
 * @emits TimersMonitor#loaded
 */
function TimersMonitor(_workspace) {
  const self = this;

  const workspace = _workspace;

  const TRESHOLDS = {
    checkInterval: 50,
    maxChecks: 40,
    maxResets: 2,
  };

  let isRunning = false;

  let isLoaded = false;

  let checksCount = 0;

  let resetsCount = 0;

  let prevTimersCount = null;

  let checksTimer = null;

  async function _triggerTimersCheck() {
    checksTimer = clearTimeout(checksTimer);
    if (workspace.dialog() !== null) {
      self.stop();
      isLoaded = true;
      logger.warning('loaded per dialog');
      self.emit('loaded');
      return;
    }
    let timersCount = 0;
    timersCount = await workspace.evaluate(() => {
      // TODO: we should consider the timing of the meta
      const metasCounts = document._TSE_querySelectorAll('meta[http-equiv="refresh"], meta[http-equiv="Refresh"]').length;
      let domCounts = { timeouts: 0, intervals: 0 };
      if (typeof window._TSE_ !== 'undefined' && typeof window._TSE_.Monitor !== 'undefined') {
        domCounts = window._TSE_.Monitor.counts();
      }
      return domCounts.timeouts + domCounts.intervals + metasCounts;
    }).catch(() => { timersCount = 0; });
    logger.debug('counts:', timersCount, prevTimersCount, checksCount, resetsCount, 'verbose');
    if (prevTimersCount !== null && (!timersCount || checksCount >= TRESHOLDS.maxChecks)) {
      self.stop();
      isLoaded = true;
      if (checksCount >= TRESHOLDS.maxChecks) {
        // In real world this scenario is normal
        logger.warning('loaded per max checks');
      } else {
        logger.info('loaded');
      }
      self.emit('loaded');
      return;
    } else if (prevTimersCount !== null && prevTimersCount < timersCount && resetsCount < TRESHOLDS.maxResets) {
      checksTimer = clearTimeout(checksTimer);
      checksCount = 0;
      resetsCount++;
      setTimeout(() => {
        checksTimer = setTimeout(_triggerTimersCheck, TRESHOLDS.checkInterval);
      }, (TRESHOLDS.checkInterval * 10) - TRESHOLDS.checkInterval);
    } else if (prevTimersCount === timersCount) {
      checksCount++;
    }
    prevTimersCount = timersCount;
    checksTimer = setTimeout(_triggerTimersCheck, TRESHOLDS.checkInterval);
  }

  /**
   * Starts the monitoring
   */
  this.start = () => {
    if (isRunning) {
      self.stop();
    }
    isRunning = true;
    isLoaded = false;
    checksCount = 0;
    resetsCount = 0;
    prevTimersCount = null;
    checksTimer = setTimeout(_triggerTimersCheck, TRESHOLDS.checkInterval);
    logger.debug('started');
    return true;
  };

  /**
   * Stops the monitoring
   */
  this.stop = () => {
    checksTimer = clearTimeout(checksTimer);
    if (!isRunning) {
      logger.warning('stop not possible, monitor is already stopped.', 'verbose');
      return false;
    }
    isRunning = false;
    logger.debug('stopped');
    return true;
  };

  /**
   * Pauses the monitoring
   */
  this.pause = self.stop;

  /**
   * Resumes the monitoring
   */
  this.resume = self.start;

  /**
   * Returns if the monitoring is in progress
   * @return {boolean}
   */
  this.isRunning = () => isRunning;

  /**
   * Returns if the monitoring is and completed
   * @return {boolean}
   */
  this.isLoaded = () => isLoaded;

  /**
   * Defines and returns the current configuration
   * @param {TimersMonitorConf} [conf] If passed, the configuration will be updated.
   * @return {TimersMonitorConf}
   */
  this.configuration = (conf) => {
    if (typeof conf === 'object') {
      if (typeof conf.checkInterval === 'number') {
        TRESHOLDS.checkInterval = parseInt(conf.checkInterval, 10);
      }
      if (typeof conf.maxChecks === 'number') {
        TRESHOLDS.maxChecks = parseInt(conf.maxChecks, 10);
      }
      if (typeof conf.maxResets === 'number') {
        TRESHOLDS.maxChecks = parseInt(conf.maxResets, 10);
      }
    }
    return {
      checkInterval: TRESHOLDS.checkInterval,
      maxChecks: TRESHOLDS.maxChecks,
      maxResets: TRESHOLDS.maxResets,
    };
  };

  return self;
}

inherits(TimersMonitor, EventEmitter);

/**
 * This event is emitted when the monitoring is started and completed properly,
 * it means that the timers count is 0 or the max check treshold is reached.
 * @event TimersMonitor#loaded
 */

/**
 * @typedef {Object} TimersMonitorConf
 * @property {number} [checkInterval] The amount of time, in milliseconds, to pass between each execution of the loaded checker. Defaults to 50.
 * @property {number} [maxChecks] The amount of checks before the monitor will be considered as loaded anyway. Defaults to 40.
 * @property {number} [maxResets] The amount of times the monitoring can be resetted because the count of timers is increased. Defaults to 2.
 */

module.exports = TimersMonitor;
