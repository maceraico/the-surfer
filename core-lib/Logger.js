const { inherits } = require('util');
const EventEmitter = require('events');

/**
 * Logger is a manager of clients that provide methods to print different types of logs (debug, info, success, warning, error)
 * on standard output with the ability to filter them by level (normal, verbose, dev). This class allows you to manage globally
 * the format of logs.
 * @constructor
 */
function Logger() {
  const self = this;

  const iconsMap = {
    info: 'ℹ️ ',
    success: '✅',
    warning: '⚠️ ',
    error: '❌',
    debug: '➖',
  };

  const validLevels = ['normal', 'verbose', 'dev'];

  let defaultVerbosity = 'normal';

  let clients = {};

  let clientsVerbosity = {};

  let printDate = true;

  let printIcon = true;

  function _applyClientVerbosity(name) {
    if (typeof clients[name] !== 'undefined') {
      const verbosity = typeof clientsVerbosity[name] === 'string' ? clientsVerbosity[name] : defaultVerbosity;
      if (verbosity === 'dev') {
        clients[name].development(true);
      } else if (verbosity === 'verbose') {
        clients[name].development(false);
        clients[name].verbose(true);
      } else {
        clients[name].development(false);
        clients[name].verbose(false);
      }
    }
  }

  function _print(type, args) {
    args.unshift(`[${this.name()}]`);
    if (printDate) {
      const now = new Date();
      let hours = now.getUTCHours().toString();
      let minutes = now.getUTCMinutes().toString();
      let seconds = now.getUTCSeconds().toString();
      let milli = now.getUTCMilliseconds().toString();
      if (hours.length === 1) {
        hours = `0${hours}`;
      }
      if (minutes.length === 1) {
        minutes = `0${minutes}`;
      }
      if (seconds.length === 1) {
        seconds = `0${seconds}`;
      }
      if (milli.length === 1) {
        milli = `00${milli}`;
      } else if (milli.length === 2) {
        milli = `0${milli}`;
      }
      args.unshift(`${hours}:${minutes}:${seconds}.${milli}`);
    }
    if (printIcon) {
      args.unshift(iconsMap[type]);
    }
    console.log.apply(self, args); // eslint-disable-line no-console
  }

  /**
   * Returns the instance of the client associated to the name passed as parameter.
   * @param {string} name
   * @param {string} [verbosity] The verbosity level to apply to the client. Defaults to normal.
   * @return {LoggerClient}
   */
  this.client = (_name, verbosity) => {
    const name = typeof _name === 'string' ? _name.trim() : '';
    if (!name) {
      throw new Error('A valid client name is required.');
    }
    if (typeof verbosity === 'string' && validLevels.indexOf(verbosity) > -1) {
      clientsVerbosity[name] = verbosity;
    }
    if (typeof clients[name] === 'undefined') {
      clients[name] = new LoggerClient(name);
      _applyClientVerbosity(name);
      clients[name].on('message', _print);
    }
    return clients[name];
  };

  /**
   * Deletes the instance of the client associated to name passed as parameter.
   * @param {string} name
   * @returns true if the client has been deleted, false otherwise.
   */
  this.remove = (_name) => {
    const name = typeof _name === 'string' ? _name.trim() : '';
    if (!name || typeof clients[name] === 'undefined') {
      return false;
    }
    delete clients[name];
    return true;
  };

  /**
   * Deletes all instances of clients.
   */
  this.flush = () => {
    clients = {};
    return true;
  };

  /**
   * Defines and returns if the current date will be printed.
   * @param {boolean} [flag] If passed, it will be applied.
   * @return {boolean} true if the date will be printed, false otherwise.
   */
  this.printDate = (flag) => {
    if (typeof flag === 'undefined') {
      printDate = !!flag;
    }
    return printDate;
  };

  /**
   * Defines and returns if the icon associeted to the type of log will be printed.
   * @param {boolean} [flag] If passed, it will be applied.
   * @return {boolean} true if the icon will be printed, false otherwise.
   */
  this.printIcon = (flag) => {
    if (typeof flag === 'undefined') {
      printIcon = !!flag;
    }
    return printIcon;
  };

  /**
   * Defines and returns the current configuration.
   * @param {LoggerConf} [conf] If passed, the configuration will be updated.
   * @return {LoggerConf}
   */
  this.configuration = (conf) => {
    if (typeof conf.verbosity === 'string' && validLevels.indexOf(conf.verbosity) > -1) {
      defaultVerbosity = conf.verbosity;
    }
    if (typeof conf.printDate === 'boolean') {
      ({ printDate } = conf);
    }
    if (typeof conf.printIcon === 'boolean') {
      ({ printIcon } = conf);
    }
    clientsVerbosity = {};
    if (typeof conf.clientsVerbosity === 'object') {
      const names = Object.keys(conf.clientsVerbosity);
      names.forEach((name) => {
        const verbosity = conf.clientsVerbosity[name];
        if (typeof verbosity === 'string' && validLevels.indexOf(verbosity) > -1) {
          clientsVerbosity[name] = verbosity;
          _applyClientVerbosity(name);
        }
      });
    }
    return {
      verbosity: defaultVerbosity,
      printDate,
      printIcon,
      clientsVerbosity,
    };
  };

  return self;
}

/**
 * LoggerClient provides methods to print different logs and to manage the current verbosity level.
 * @param {string} name The name of client.
 * @constructor
 */
function LoggerClient(_name) {
  const self = this;

  const name = _name;

  let isVerbose = false;

  let development = false;

  function _parseArgs(args) {
    let argsCount = args.length;
    let verbose = false;
    const lastArg = args[argsCount - 1];

    if (!development && lastArg === 'dev') {
      return { args: [], verbose: false };
    }

    if (lastArg === 'verbose' || lastArg === 'dev') {
      args.pop();
      argsCount -= 1;
      verbose = true;
    }

    return { args, verbose };
  }

  /**
   * Returns the name of the client.
   * @returns {string}
   */
  this.name = () => name;

  /**
   * Defines and returns if verbose logs will be printed.
   * @param {boolean} [flag] If true, the verbosity level will be setted to verbose.
   * @return {boolean}
   */
  this.verbose = (flag) => {
    if (typeof flag !== 'undefined') {
      isVerbose = !!flag;
    }
    return isVerbose;
  };

  /**
   * Defines and returns if development logs will be printed. By setting the verbosity level to development the verbose logs
   * will be also printed.
   * @param {boolean} [flag] If true, the verbosity level will be setted to development.
   * @return {boolean}
   */
  this.development = (flag) => {
    if (typeof flag !== 'undefined') {
      development = !!flag;
      if (development) {
        isVerbose = true;
      }
    }
    return development;
  };

  /**
   * Prints a debug log. The last parameter will be considered as the verbosity level of the log by passing one of
   * these values: normal, verbose, dev.
   * @param {...*} args these arguments will be passed to the print function in order to print them on standard output.
   */
  this.debug = (...args) => {
    const parsedArgs = _parseArgs(args);
    if (!parsedArgs.args.length || (!isVerbose && parsedArgs.verbose)) {
      return;
    }
    self.emit('message', 'debug', parsedArgs.args);
  };

  /**
   * Prints a info log. The last parameter will be considered as the verbosity level of the log by passing one of
   * these values: normal, verbose, dev.
   * @param {...*} args these arguments will be passed to the print function in order to print them on standard output.
   */
  this.info = (...args) => {
    const parsedArgs = _parseArgs(args);
    if (!parsedArgs.args.length || (!isVerbose && parsedArgs.verbose)) {
      return;
    }
    self.emit('message', 'info', parsedArgs.args);
  };

  /**
   * Prints a success log. The last parameter will be considered as the verbosity level of the log by passing one of
   * these values: normal, verbose, dev.
   * @param {...*} args these arguments will be passed to the print function in order to print them on standard output.
   */
  this.success = (...args) => {
    const parsedArgs = _parseArgs(args);
    if (!parsedArgs.args.length || (!isVerbose && parsedArgs.verbose)) {
      return;
    }
    self.emit('message', 'success', parsedArgs.args);
  };

  /**
   * Prints a warning log. The last parameter will be considered as the verbosity level of the log by passing one of
   * these values: normal, verbose, dev.
   * @param {...*} args these arguments will be passed to the print function in order to print them on standard output.
   */
  this.warning = (...args) => {
    const parsedArgs = _parseArgs(args);
    if (!parsedArgs.args.length || (!isVerbose && parsedArgs.verbose)) {
      return;
    }
    self.emit('message', 'warning', parsedArgs.args);
  };

  /**
   * Prints a error log. The last parameter will be considered as the verbosity level of the log by passing one of
   * these values: normal, verbose, dev.
   * @param {...*} args these arguments will be passed to the print function in order to print them on standard output.
   */
  this.error = (...args) => {
    const parsedArgs = _parseArgs(args);
    if (!parsedArgs.args.length || (!isVerbose && parsedArgs.verbose)) {
      return;
    }
    self.emit('message', 'error', parsedArgs.args);
  };

  return self;
}

inherits(LoggerClient, EventEmitter);

/**
 * @typedef {Object} LoggerConf
 * @property {string} [verbosity] The defalut verbosity level to apply on clients. Possible values are: normal, verbose, dev. Defaults to normal.
 * @property {boolean} [printDate] Defines if the current date will be printed. Defaults to true.
 * @property {boolean} [printIcon] Defines if the icon associeted to the type of log will be printed. Defaults to true.
 * @property {Object} [clientsVerbosity] A map where the keys will be considered as a client name and the values as the verbosity level to apply.
 */

module.exports = this instanceof Logger ? this : new Logger();
